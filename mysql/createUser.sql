CREATE USER 'rental_user'@'localhost' IDENTIFIED BY 'diplomant';
GRANT ALL PRIVILEGES ON rentapp . * TO 'rental_user'@'localhost' IDENTIFIED BY 'diplomant';
GRANT SELECT ON * . * TO 'rental_user'@'localhost';
FLUSH PRIVILEGES;
