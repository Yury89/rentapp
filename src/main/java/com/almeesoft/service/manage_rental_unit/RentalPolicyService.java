package com.almeesoft.service.manage_rental_unit;

import com.almeesoft.entities.Policy;
import com.almeesoft.entities.RentalPolicy;
import com.almeesoft.entities.RentalUnit;
import com.almeesoft.repository.PoliceRepository;
import com.almeesoft.repository.manage_rental_unit.RentalPolicyRepository;
import com.almeesoft.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Iterator;
import java.util.List;

@Service
public class RentalPolicyService {

    @Autowired
    private RentalPolicyRepository rentalPolicyRepository;

    @Autowired
    private PoliceRepository policeRepository;


    @Autowired
    private RentalUnitService rentalUnitService;

    public List<Policy> getNotCheckedPolicy(Long idRental) {

        List<Policy> globalPolicy = policeRepository.getGlobalPolicy();
        Iterator<Policy> iterator = globalPolicy.iterator();
        while (iterator.hasNext()) {
            Policy next = iterator.next();
            if (rentalPolicyRepository.checkPolicy(idRental, next.getId()))
                iterator.remove();
        }

        return globalPolicy;

    }

    public void saveRentalAmenityToRentalUnit(Long idRental, Long[] idPolicy, String usersPolicy) {

        RentalUnit current = rentalUnitService.getById(idRental);
        current.setPolicies(usersPolicy);
        rentalUnitService.updateUsersPolicy(idRental,usersPolicy);
        for (Long aLong : idPolicy) {
            RentalPolicy rentalPolicy = new RentalPolicy();
            rentalPolicy.setPolicy(policeRepository.getPoliceById(aLong));
            rentalPolicy.setRentalUnit(current);
            rentalPolicyRepository.save(rentalPolicy);
        }

    }
}
