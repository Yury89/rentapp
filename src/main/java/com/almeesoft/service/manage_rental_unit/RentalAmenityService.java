package com.almeesoft.service.manage_rental_unit;

import com.almeesoft.entities.RentalAmenity;
import com.almeesoft.entities.RentalUnit;
import com.almeesoft.repository.AmenityRepository;
import com.almeesoft.repository.manage_rental_unit.RentalAmenityRepository;
import com.almeesoft.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

@Service
public class RentalAmenityService extends RentalAmenityRepository {

    @Autowired
    private RentalUnitService rentalUnitService;

    @Autowired
    private AmenityRepository amenityRepository;

    public void saveRentalAmenityToRentalUnit(Long idRentalUnit, Long[] idAmenities, BigDecimal[] prices){

        RentalUnit byId = rentalUnitService.getById(idRentalUnit);

        for (int i = 0; i < idAmenities.length; i++) {
            RentalAmenity rentalAmenity = new RentalAmenity();
            rentalAmenity.setAmenity(amenityRepository.getAmenity(idAmenities[i]));
            rentalAmenity.setPrice(prices[i]);
            rentalAmenity.setRentalUnit(byId);
            save(rentalAmenity);
        }
    }

}
