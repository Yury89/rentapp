package com.almeesoft.service.manage_rental_unit;

import com.almeesoft.entities.Amenities;
import com.almeesoft.entities.RentalUnit;
import com.almeesoft.repository.AmenityRepository;
import com.almeesoft.repository.manage_rental_unit.RentalAmenityRepository;
import com.almeesoft.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AmenityService {

    @Autowired
    private RentalAmenityRepository rentalAmenityRepository;

    @Autowired
    private AmenityRepository amenityRepository;

    @Autowired
    private RentalUnitService rentalUnitService;


    public List<Amenities> getAmenityForManage(Long idUser, Long idRental) {

        List<Amenities> amenitiesForView = new ArrayList<Amenities>();
        if(idRental != null)
           amenitiesForView = rentalUnitService.getAmenitiesForView(rentalUnitService.getById(idRental));

        List<Amenities> globalAmenities = amenityRepository.getGlobalAmenities();
        globalAmenities.addAll(amenityRepository.getUsersAmenities(idUser));

        for (Amenities next : globalAmenities) {

            int i = amenitiesForView.indexOf(next);
            if(i != -1) {
                next.setParent(true);
                next.setPrice(amenitiesForView.get(i).getPrice());
            }
            if(idRental != null)
            if(!next.getParent())
            if (rentalAmenityRepository.check(idRental, next.getId())) {

                    next.setChecked(true);
                    next.setPrice(amenityRepository.getPrice(next.getId(), idRental));

            }
        }
        return globalAmenities;
    }

}
