package com.almeesoft.service.mysqldump;

import org.springframework.stereotype.Service;

@Service
public class MysqlDumpService {

    public void backUp(){

        Process p = null;
        try {

            Runtime runtime = Runtime.getRuntime();
            p = runtime.exec("mysqldump -u rental_user -p diplomant > ./dump.sql");
            int processComplete = p.waitFor();

            if (processComplete == 0) {

                System.out.println("Backup created successfully!");

            } else {
                System.out.println("Could not create the backup");
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
