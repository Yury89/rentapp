package com.almeesoft.service;

import com.almeesoft.entities.*;
import com.almeesoft.repository.AmenityRepository;
import com.almeesoft.repository.PoliceRepository;
import com.almeesoft.repository.RentalUnitRepositoryImpl;
import com.almeesoft.repository.manage_rental_unit.ImageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Service
public class RentalUnitServiceImpl extends RentalUnitRepositoryImpl implements RentalUnitService {


    @Autowired
    private ImageService imageService;

    @Autowired
   private ImageRepository imageRepository;

    @Autowired
    private AmenityRepository amenityRepository;

    @Autowired
    private PoliceRepository policeRepository;


    @Override
    public List<RentalUnit> getParentRentalUnitsById(long id) {
        return getById(id).getSunRentalUnits();


    }

    @Override
    public List<Amenities> getAllAmenities(List<RentalUnit> rentalUnits) {
        Iterator<RentalUnit> iterator = rentalUnits.iterator();
        ArrayList<Amenities> amenities = new ArrayList<Amenities>();
        while (iterator.hasNext()) {
            List<RentalAmenity> rentalAmenities = iterator.next().getRentalAmenities();
            for (RentalAmenity amenity : rentalAmenities) {
                amenities.add(amenity.getAmenity());
            }

        }
        return amenities;
    }

    @Override
    public List<Policy> getAllPolices(List<RentalUnit> rentalUnits) {
        Iterator<RentalUnit> iterator = rentalUnits.iterator();
        ArrayList<Policy> amenities = new ArrayList<Policy>();
        while (iterator.hasNext()) {
            List<RentalPolicy> rentalAmenities = iterator.next().getRentalPolicies();
            for (RentalPolicy amenity : rentalAmenities) {
                amenities.add(amenity.getPolicy());
            }

        }
        return amenities;
    }

    //Amenities,Policy,Images, Price, Address
    @Override
    public void saveImageAndRentalUnit(RentalUnit rentalUnit, List<MultipartFile> files) throws IOException {
        rentalUnit.setImages(new ArrayList<Image>());
        for (MultipartFile multipartFile : files) {
            rentalUnit.getImages().add(new Image(imageService.compressImage(multipartFile), rentalUnit));
        }
        addIdToRental(rentalUnit);
        addIdToProperties(rentalUnit);
        save(rentalUnit);
        imageService.uploadImage(rentalUnit);
    }

    @Override
    public void updateRentalUnit(RentalUnit rentalUnit) {
        executeWithNamedParameters("RENTAL_UNIT.update",new String[]{"name","title","description","id"},new Object[]{rentalUnit.getName(),rentalUnit.getTitle(),rentalUnit.getDescription(),rentalUnit.getId()});
    }

    @Override
    public void update(RentalUnit rentalUnit) {
        super.execOrUpdate(rentalUnit);
    }

    @Override
    public void updateRentalUnitforManage(RentalUnit rentalUnit,Long[] idsForImages, List<MultipartFile> files) throws IOException {
        if(rentalUnit.getAddress() != null)
        rentalUnit.getAddress().setRentalUnit(rentalUnit);
        rentalUnit.getPrice().setRentalUnit(rentalUnit);
        List<Image> images = new ArrayList<Image>();
        addIdToRental(rentalUnit);
        if(idsForImages != null)
        for (Long idsForImage : idsForImages) {
            imageRepository.deleteImage(idsForImage);
            imageService.delImage(idsForImage);
        }
        for (MultipartFile file : files) {

            File file1 = imageService.compressImage(file);
            images.add(new Image(file1,rentalUnit));
        }
        rentalUnit.setImages(images);

        policeRepository.dellPolicy(rentalUnit.getId());
        amenityRepository.dellAmenity(rentalUnit.getId());
        update(rentalUnit);
        imageService.insertImage(images);
    }

    @Override
    public List<String> getFullAddresses(RentalUnit rentalUnit) {
        ArrayList<String> strings = new ArrayList<String>();
        while (rentalUnit.getParentRentalUnit() != null){
            rentalUnit = rentalUnit.getParentRentalUnit();
            strings.add(0,rentalUnit.getFullAddress());
        }
        return strings;
    }

    @Override
    public List<Amenities> getAmenitiesForView(RentalUnit currentRentalUnit) {
        ArrayList<Amenities> amenities = new ArrayList<Amenities>();
        while (currentRentalUnit.getParentRentalUnit() != null){
            currentRentalUnit = currentRentalUnit.getParentRentalUnit();
            if(currentRentalUnit.getRentalAmenities() != null)
                for (RentalAmenity rentalAmenity : currentRentalUnit.getRentalAmenities()) {
                    Amenities tmp = new Amenities();
                    tmp.setTitle(rentalAmenity.getAmenity().getTitle());
                    tmp.setPrice(rentalAmenity.getPrice());
                    amenities.add(tmp);
                }
        }

        return amenities;
    }

    @Override
    public List<Policy> getPoliciesForView(RentalUnit currentRentalUnit) {
        ArrayList<Policy> policies = new ArrayList<Policy>();
        while (currentRentalUnit.getParentRentalUnit() != null){
            currentRentalUnit = currentRentalUnit.getParentRentalUnit();
            if(currentRentalUnit.getRentalPolicies() != null)
                for (RentalPolicy rentalPolicy : currentRentalUnit.getRentalPolicies()) {
                    policies.add(rentalPolicy.getPolicy());
                }
        }

        return policies;
    }

    @Override
    public Address getMainAddress(Long idRental) {
        RentalUnit current = getById(idRental);
        if(current.getParentRentalUnit() == null)
            return current.getAddress();
        if(current.getParentRentalUnit() == null)
            return current.getAddress();
        while (current.getParentRentalUnit() != null){
            current = current.getParentRentalUnit();

        }
        return current.getAddress();
    }

    private void addIdToRental(RentalUnit rentalUnit){
        if(rentalUnit.getRentalAmenities() != null)
//            for (RentalAmenity rentalAmenity : rentalUnit.getRentalAmenities()) {
//            if(rentalAmenity.getAmenity() != null)
//                rentalAmenity.setRentalUnit(rentalUnit);
//
//            }
        {
            Iterator<RentalAmenity> iterator = rentalUnit.getRentalAmenities().iterator();

            while (iterator.hasNext()){
                RentalAmenity rentalAmenity = iterator.next();
                if(rentalAmenity.getAmenity() != null)
                    rentalAmenity.setRentalUnit(rentalUnit);
                else iterator.remove();
            }
        }
        if(rentalUnit.getRentalPolicies() != null) {
            Iterator<RentalPolicy> iterator = rentalUnit.getRentalPolicies().iterator();

            while (iterator.hasNext()){
                RentalPolicy rentalPolicy = iterator.next();
                if(rentalPolicy.getPolicy() != null)
                    rentalPolicy.setRentalUnit(rentalUnit);
                else iterator.remove();
            }
        }
    }

    private void addIdToProperties(RentalUnit rentalUnit) {
//        if(rentalUnit.getRentalPolicies() != null)
//        for (RentalPolicy policy : rentalUnit.getRentalPolicies()) {
//            policy.setRentalUnit(rentalUnit);
//        }
//        if(rentalUnit.getRentalAmenities() != null)
//        for (RentalAmenity rentalAmenity : rentalUnit.getRentalAmenities()) {
//            rentalAmenity.setRentalUnit(rentalUnit);
//        }
        rentalUnit.getPrice().setRentalUnit(rentalUnit);
        if(rentalUnit.getAddress() != null)
        rentalUnit.getAddress().setRentalUnit(rentalUnit);
    }

    private List<RentalAmenity> getAmenities(RentalUnit rentalUnits, Long[] idAmities) {
        List<RentalAmenity> amenities = new ArrayList<RentalAmenity>();
        if (idAmities == null)
            return amenities;
        for (Long idAmity : idAmities) {
            amenities.add(new RentalAmenity(rentalUnits, amenityRepository.getAmenity(idAmity)));
        }
        return amenities;
    }


    private List<RentalPolicy> getPolices(RentalUnit rentalUnits, Long[] idPolices) {


        List<RentalPolicy> amenities = new ArrayList<RentalPolicy>();
        if (idPolices == null)
            return amenities;
        for (Long idPolice : idPolices) {
            amenities.add(new RentalPolicy(rentalUnits, policeRepository.getPoliceById(idPolice)));
        }
        return amenities;
    }







}
