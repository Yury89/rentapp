package com.almeesoft.service;

import com.almeesoft.entities.Address;
import com.almeesoft.entities.Amenities;
import com.almeesoft.entities.Policy;
import com.almeesoft.entities.RentalUnit;
import com.almeesoft.repository.RentalUnitRepository;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.util.List;

public interface RentalUnitService extends RentalUnitRepository {

    List<RentalUnit> getParentRentalUnitsById(long id);

    List<Amenities> getAllAmenities(List<RentalUnit> rentalUnits);

    List<Policy> getAllPolices(List<RentalUnit> rentalUnits);

    void saveImageAndRentalUnit(RentalUnit rentalUnit, List<MultipartFile> files) throws IOException;

    void updateRentalUnit(RentalUnit rentalUnit);

    void update(RentalUnit rentalUnit);

    void updateRentalUnitforManage(RentalUnit rentalUnit, Long[] idsForImages, List<MultipartFile> files) throws IOException;

    List<String> getFullAddresses(RentalUnit rentalUnit);

    List<Amenities> getAmenitiesForView(RentalUnit currentRentalUnit);

    List<Policy> getPoliciesForView(RentalUnit currentRentalUnit);


    Address getMainAddress(Long idRental);




}
