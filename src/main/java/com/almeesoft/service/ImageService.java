package com.almeesoft.service;

import com.almeesoft.entities.Image;
import com.almeesoft.entities.RentalUnit;
import com.mongodb.gridfs.GridFSDBFile;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.gridfs.GridFsCriteria;
import org.springframework.data.mongodb.gridfs.GridFsTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import javax.imageio.IIOImage;
import javax.imageio.ImageIO;
import javax.imageio.ImageWriteParam;
import javax.imageio.ImageWriter;
import javax.imageio.stream.ImageOutputStream;
import java.awt.image.BufferedImage;
import java.io.*;
import java.util.Iterator;
import java.util.List;

@Service
public class ImageService {

    @Autowired
    private GridFsTemplate gridFsTemplate;




    public void uploadImage(RentalUnit rentalUnit) {
        for (Image image : rentalUnit.getImages()) {
            try {
                gridFsTemplate.store(new FileInputStream(image.getMultipartFile()), String.valueOf(image.getId()), "jpg");


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void insertImage(List<Image> images) {
        for (Image image : images) {
            try {
                gridFsTemplate.store(new FileInputStream(image.getMultipartFile()), String.valueOf(image.getId()), "jpg");


            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public GridFSDBFile getImage(String name){

        return gridFsTemplate.findOne(new Query().addCriteria(Criteria.where("filename").is(name)));

    }


    public File compressImage(MultipartFile multipartFile) throws IOException {

        File compressedImageFile = new File(multipartFile.getOriginalFilename());

        InputStream inputStream = multipartFile.getInputStream();
        OutputStream outputStream = new FileOutputStream(compressedImageFile);

        float imageQuality = 0.5f;

        //Create the buffered image
        BufferedImage bufferedImage = ImageIO.read(inputStream);

        //Get image writers
        Iterator<ImageWriter> imageWriters = ImageIO.getImageWritersByFormatName("jpg");


        ImageWriter imageWriter = imageWriters.next();
        ImageOutputStream imageOutputStream = ImageIO.createImageOutputStream(outputStream);
        imageWriter.setOutput(imageOutputStream);

        ImageWriteParam imageWriteParam = imageWriter.getDefaultWriteParam();

        //Set the compress quality metrics
        imageWriteParam.setCompressionMode(ImageWriteParam.MODE_EXPLICIT);
        imageWriteParam.setCompressionQuality(imageQuality);

        //Created image
        imageWriter.write(null, new IIOImage(bufferedImage, null, null), imageWriteParam);

        // close all streams
        inputStream.close();
        outputStream.close();
        imageOutputStream.close();
        imageWriter.dispose();

        return compressedImageFile;
    }

    public void delImage(Long id){
        Query query = new Query(GridFsCriteria.whereFilename().is(id));
        gridFsTemplate.delete(query);
    }
}


