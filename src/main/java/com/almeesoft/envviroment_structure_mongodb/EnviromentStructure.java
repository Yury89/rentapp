package com.almeesoft.envviroment_structure_mongodb;

public class EnviromentStructure {

    private String appName;

    private String MongoDBUrl;

    private String MysqlHost;

    private String MysqlPort;

    private String MysqlUsername;

    private String MysqlPassword;

    public EnviromentStructure(String appName, String mongoDBUrl, String mysqlHost, String mysqlPort, String mysqlUsername, String mysqlPassword) {
        this.appName = appName;
        MongoDBUrl = mongoDBUrl;
        MysqlHost = mysqlHost;
        MysqlPort = mysqlPort;
        MysqlUsername = mysqlUsername;
        MysqlPassword = mysqlPassword;
    }

    public String getAppName() {

        return appName;
    }

    public void setAppName(String appName) {
        this.appName = appName;
    }

    public String getMongoDBUrl() {
        return MongoDBUrl;
    }

    public void setMongoDBUrl(String mongoDBUrl) {
        MongoDBUrl = mongoDBUrl;
    }

    public String getMysqlHost() {
        return MysqlHost;
    }

    public void setMysqlHost(String mysqlHost) {
        MysqlHost = mysqlHost;
    }

    public String getMysqlPort() {
        return MysqlPort;
    }

    public void setMysqlPort(String mysqlPort) {
        MysqlPort = mysqlPort;
    }

    public String getMysqlUsername() {
        return MysqlUsername;
    }

    public void setMysqlUsername(String mysqlUsername) {
        MysqlUsername = mysqlUsername;
    }

    public String getMysqlPassword() {
        return MysqlPassword;
    }

    public void setMysqlPassword(String mysqlPassword) {
        MysqlPassword = mysqlPassword;
    }
}
