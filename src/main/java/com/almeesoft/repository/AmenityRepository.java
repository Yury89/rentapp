package com.almeesoft.repository;

import org.springframework.stereotype.Repository;
import com.almeesoft.entities.Amenities;
import com.almeesoft.repository.generic.GenericRepositoryImpl;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class AmenityRepository extends GenericRepositoryImpl<Amenities> {


    public List<Amenities> getAllAmenity() {
        return findByNamedQuery("getAllAmenities");
    }

    public Amenities getAmenity(Long id) {
        return findByNamedQueryParameter("getAmenitiesById", "id", id).get(0);
    }

    public void saveAmenity(Amenities amenities) {
        save(amenities);
    }

    public List<Amenities> getGlobalAmenities() {

        return findByNamedQuery("AMENITY.getGlobalAmenities");
    }

    public int deleteAmenity(Long id) {
        executeOrUpdate("RENTALAMENITY.delete", "id", id);
        return executeOrUpdate("AMENITY.delete", "id", id);
    }

    public int updateAmenity(Long id, String value) {

        return updateWithNamedParameters("AMENITY.update", new String[]{"title", "id"}, new Object[]{value, id});
    }

    public List<Amenities> getUsersAmenities(Long userId){
        return findByNamedQueryParameter("AMENITY.getUsersAmenity","id",userId);
    }

    public int updateAmenityDescription(Long id,String value){
        return updateWithNamedParameters("AMENITY.updateDescription",new String[]{"description","id"},new Object[]{value,id});
    }

    public boolean isChecked(Long id){
        return checking("AMENITY.isChecked",new String[]{"id"},new Object[]{id});
    }

    public BigDecimal getPrice(Long amenityId, Long rentalId){
        return (BigDecimal) getObjectWithNamedParameters("AMENITY.getPrice",new String[]{"amenityId","rentalId"},new Object[]{amenityId,rentalId});
    }

    public void dellAmenity(Long id){
        executeOrUpdate("RENTAL_AMENITY.deleteAll","id",id);
    }

}
