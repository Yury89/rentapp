package com.almeesoft.repository;

import com.almeesoft.entities.RentalUnit;

import java.util.List;

public interface RentalUnitRepository {

    List<RentalUnit> getMainRentalUnits();


    List<RentalUnit> getChildrenRentalUnitsById(long id);

    RentalUnit getById(long id);

    List<RentalUnit> getAllRentalUnit();

    List<RentalUnit> getUsersRentalUnit(Long id);

    List<RentalUnit> getRentalUnitForManage(Long currentId);


    int updateUsersPolicy(Long idRental,String val);





}
