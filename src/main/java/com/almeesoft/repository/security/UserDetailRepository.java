package com.almeesoft.repository.security;

import com.almeesoft.entities.User;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Repository;

@Repository
public class UserDetailRepository extends GenericRepositoryImpl<User> {
    public UserDetails getUserDetailsByLogin(String login){
        return findByNamedQueryParameter("USER.GetUserByName","login",login).get(0);
    }
}
