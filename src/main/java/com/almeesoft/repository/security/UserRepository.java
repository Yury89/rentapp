package com.almeesoft.repository.security;

import com.almeesoft.entities.User;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepository extends GenericRepositoryImpl<User>{

    public List<User>getAllUsers(){
        return findByNamedQuery("USER.getAllUsers");
    }
}
