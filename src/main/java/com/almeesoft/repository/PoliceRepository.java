package com.almeesoft.repository;

import com.almeesoft.entities.Amenities;
import com.almeesoft.entities.Policy;
import com.almeesoft.entities.RentalUnit;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import com.almeesoft.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class PoliceRepository extends GenericRepositoryImpl<Policy> {

    @Autowired
    private RentalUnitService rentalUnitService;



    public List<Policy> getAllPolices(){
       return findByNamedQuery("getAllPolices");
    }

    public Policy getPoliceById(Long idPolice) {
        return findByNamedQueryParameter("getPoliceById","id",idPolice).get(0);
    }

    public  void savePolice(Policy policy){
        save(policy);
    }

    public void deleteAmenity(Long id) {
        executeOrUpdate("RENTAL_POLICY","id",id);
        executeOrUpdate("POLICY.delete","id",id);
    }

    public int updateAmenity(Long id, String name) {
        return updateWithNamedParameters("POLICY.update",new String[]{"name","id"},new Object[]{name,id});
    }

    public List<Policy> getGlobalPolicy(){
        return findByNamedQuery("POLICY.getGlobalPolicy");
    }

    public List<Policy> getUsersPolicy(Long id){
        return findByNamedQueryParameter("POLICY,getUsersPolicy","id",id);
    }

    public int updatePolicyDescription(Long id,String value){
        return updateWithNamedParameters("POLICY.updateDescription",new String[]{"description","id"},new Object[]{value,id});
    }
    public Boolean check(Long policyId, Long rentalId){
        return checking("POLICY.check",new String[]{"idRental","idPolicy"},new Object[]{rentalId,policyId});
    }

    public List<Policy> getPolicyForManage(Long idRental){

        List<Policy> policiesForView = rentalUnitService.getPoliciesForView(rentalUnitService.getById(idRental));
        List<Policy> allPolices = getAllPolices();
        for (Policy allPolice : allPolices) {
            if(policiesForView.contains(allPolice))
                allPolice.setParent(true);
            if(!allPolice.getParent())
            if(check(allPolice.getId(),idRental))
                allPolice.setChecked(true);
        }
        return allPolices;
    }

    public void dellPolicy(Long id){
        executeOrUpdate("RENTAL_POLICY.deleteAll","id",id);
    }


}
