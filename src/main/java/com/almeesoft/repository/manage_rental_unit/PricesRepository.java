package com.almeesoft.repository.manage_rental_unit;


import com.almeesoft.entities.Price;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class PricesRepository extends GenericRepositoryImpl<Price> {


    public Price getPriceBy(Long id) {
        return findByNamedQueryParameter("PRICE.getPriceById","id",id).get(0);
    }

    public void updatePrice(Price price) {
         executeWithNamedParameters("PRICE.updateData",new String[]{"day","week","month","id"},new Object[]{price.getDay(),price.getWeek(),price.getMonth(),price.getRentalUnit().getId()});
    }
}
