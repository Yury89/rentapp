package com.almeesoft.repository.manage_rental_unit;

import com.almeesoft.entities.RentalPolicy;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RentalPolicyRepository extends GenericRepositoryImpl<RentalPolicy> {

    public List<RentalPolicy> getCheckedPolicy(Long id){
        return findByNamedQueryParameter("RENTAL_POLICY.getCheckedPolicyById","id",id);
    }


    public Boolean checkPolicy(Long idRental, Long idPolicy){
        return checking("RENTAL_POLICY.check",new String[]{"idRental","idPol"},new Object[]{idRental,idPolicy});
    }

    public String getUsersPolicy(Long idRental){

        return String.valueOf(findByNamedQueryParameter("RENTAL_UNIT.getUsersPolicy","id",idRental).get(0));
    }

    public int update(Long idRentalAmen, String val) {

        return executeWithNamedParameters("RENTAL_UNIT.setPolicyValue",new String[]{"policy","id"},new Object[]{val,idRentalAmen});

    }

    public int  delete(Long id) {

       return executeOrUpdate("RENTAL_POLICY.delete","id",id);

    }


}
