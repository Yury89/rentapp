package com.almeesoft.repository.manage_rental_unit;

import com.almeesoft.entities.Image;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class ImageRepository extends GenericRepositoryImpl<Image> {


    public void deleteImage(Long id){
        executeOrUpdate("IMAGE.deleteById","id",id);
    }
}
