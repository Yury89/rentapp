package com.almeesoft.repository.manage_rental_unit;

import com.almeesoft.entities.RentalAmenity;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.math.BigDecimal;
import java.util.List;

@Repository
public class RentalAmenityRepository extends GenericRepositoryImpl<RentalAmenity> {


    public List<RentalAmenity> getRentalAmenitiesFromRentalUnit(Long userId,Long idRentalUnit){

        return findByNamedQueryParameters("RENTAL_AMENITY.getAmenitiesFromRentalUnit",new String[]{"rentalId","userId"},new Object[]{idRentalUnit,userId});
    }


    public Boolean check(Long idRentalUnit, Long idAmen){
        return checking("RENTAL_AMENITY.check",new String[]{"idRental","idAmen"},new Object[]{idRentalUnit,idAmen});

    }

    public void update(Long idRentalAmen, BigDecimal val){
        updateWithNamedParameters("RENTAL_AMEN.update", new String[]{"price","id"},new Object[]{val,idRentalAmen});
    }

    public void delete(Long id){
        executeOrUpdate("RENTAL_AMEN.delete","id",id);
    }



}
