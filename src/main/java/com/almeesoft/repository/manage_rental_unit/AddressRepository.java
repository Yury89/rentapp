package com.almeesoft.repository.manage_rental_unit;

import com.almeesoft.entities.Address;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

@Repository
public class AddressRepository extends GenericRepositoryImpl<Address> {



    public Address getAddressesById(Long idRentalUnit,Long IdUSer){
        return findByNamedQueryParameters("ADDRESS.getAddressesById",new String[]{"idUser","idRentalUnit"},new Object[]{IdUSer,idRentalUnit}).get(0);
    }

    public void execOrUpdate(Address address){
        System.out.println(address.getCountry());
        super.execOrUpdate(address);
    }




}
