package com.almeesoft.repository;


import com.almeesoft.entities.RentalUnit;
import com.almeesoft.repository.generic.GenericRepositoryImpl;
import org.springframework.stereotype.Repository;

import java.util.List;


@Repository
public class RentalUnitRepositoryImpl extends GenericRepositoryImpl<RentalUnit> implements RentalUnitRepository{



    @Override
    public List<RentalUnit> getMainRentalUnits() {
        return findByNamedQuery("getMainRentalUnits");
    }


    @Override
    public List<RentalUnit> getChildrenRentalUnitsById(long id) {
        return getById(id).getSunRentalUnits();
    }

    @Override
    public RentalUnit getById(long id) {
        return findByNamedQueryParameter("getRentalUnitById","id",id).get(0);
    }

    @Override
    public List<RentalUnit> getAllRentalUnit() {
        return findByNamedQuery("getAllRentalUnit");
    }

    @Override
    public List<RentalUnit> getUsersRentalUnit(Long id) {
        return findByNamedQueryParameter("RENTAL_UNIT.getUsersRentalUnit","id",id);
    }

    @Override
    public List<RentalUnit> getRentalUnitForManage(Long currentId) {
        return findByNamedQueryParameter("RENTAL_UNIT.getUsersRentalUnitForManage","current_id",currentId);
    }

    @Override
    public int updateUsersPolicy(Long idRental, String val) {
        return executeWithNamedParameters("RENTAL_UNIT.setPolicyValue",new String[]{"policy","id"},new Object[]{val,idRental});
    }




}
