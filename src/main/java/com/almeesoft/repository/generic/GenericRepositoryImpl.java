package com.almeesoft.repository.generic;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import com.almeesoft.entities.BaseEntity;

import java.util.List;



public class GenericRepositoryImpl<T extends BaseEntity> {

    @Autowired
    private SessionFactory sessionFactory;

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected List<T> findByNamedQuery(String queryString) {
        // return entityManager.createNamedQuery(queryString).getResultList();
        return sessionFactory.openSession().getNamedQuery(queryString).list();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected List<T> findByNamedQueryParameter(String queryString, String parameterName, Object parameter) {
//        return entityManager.createNamedQuery(queryString).
//                setParameter(parameterName, parameter).getResultList();
        return sessionFactory.openSession().getNamedQuery(queryString).setParameter(parameterName, parameter).list();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected Boolean getBool(String queryString, String parameterName, Object parameter) {
//        return entityManager.createNamedQuery(queryString).
//                setParameter(parameterName, parameter).getResultList();
        return (Boolean) sessionFactory.openSession().getNamedQuery(queryString).setParameter(parameterName, parameter).uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected int executeOrUpdate(String queryString, String parameterName, Object parameter) {
//        return entityManager.createNamedQuery(queryString).
//                setParameter(parameterName, parameter).getResultList();
        return sessionFactory.openSession().getNamedQuery(queryString).setParameter(parameterName, parameter).executeUpdate();
    }

    /**
     * Convenience method for obtaining the list of objects using the named query and its parameters.
     * AMENITY.update
     */
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected Boolean checking(String queryString, String[] parameterNames, Object[] parameters) {
        Query query = sessionFactory.openSession().getNamedQuery(queryString);
        for (int i = 0; i < parameterNames.length; i++) {
            query.setParameter(parameterNames[i], parameters[i]);
        }
        return (Boolean)query.uniqueResult();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected int executeWithNamedParameters(String queryString, String[] parameterNames, Object[] parameters) {
        Query query = sessionFactory.openSession().getNamedQuery(queryString);
        for (int i = 0; i < parameterNames.length; i++) {
            query.setParameter(parameterNames[i], parameters[i]);
        }
        return query.executeUpdate();
    }

    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected List<T> findByNamedQueryParameters(String queryString, String[] parameterNames, Object[] parameters) {
        Query query = sessionFactory.openSession().getNamedQuery(queryString);
        for (int i = 0; i < parameterNames.length; i++) {
            query.setParameter(parameterNames[i], parameters[i]);
        }
        return query.list();
    }
    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected int updateWithNamedParameters(String queryString, String[] parameterNames, Object[] parameters) {
        Query query = sessionFactory.openSession().getNamedQuery(queryString);
        for (int i = 0; i < parameterNames.length; i++) {
            query.setParameter(parameterNames[i], parameters[i]);
        }
        return query.executeUpdate();
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public void save(T t){
       sessionFactory.openSession().save(t);
    }

    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    public void deleteGeneric(T t){
        sessionFactory.openSession().delete(t);


    }


    @Transactional(propagation = Propagation.REQUIRED)
    public void execOrUpdate(T t){
        Session session = sessionFactory.openSession();
        session.saveOrUpdate(t);
        session.flush();


    }


    @SuppressWarnings("unchecked")
    @Transactional(readOnly = true, propagation = Propagation.REQUIRED)
    protected Object getObjectWithNamedParameters(String queryString, String[] parameterNames, Object[] parameters) {
        Query query = sessionFactory.openSession().getNamedQuery(queryString);
        for (int i = 0; i < parameterNames.length; i++) {
            query.setParameter(parameterNames[i], parameters[i]);
        }
        return query.uniqueResult();
    }






}