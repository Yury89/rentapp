package com.almeesoft.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "PRICE")
@NamedQueries({
        @NamedQuery(name = "PRICE.getPriceById",query = "from Price where rentalUnit.id = :id"),
        @NamedQuery(name = "PRICE.updateData",query = "update Price set day = :day, week = :week, month = :month where rentalUnit.id = :id")
})
public class Price  extends BaseEntity{

    @Column(name = "day")
    private BigDecimal day;

    @Column(name = "week")
    private BigDecimal week;

    @Column(name = "month")
    private BigDecimal month;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_rental_unit")
    private RentalUnit rentalUnit;

    public boolean isPresent(){
        return !(day == null && week == null && month == null);
    }
    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public BigDecimal getDay() {
        return day;
    }

    public void setDay(BigDecimal day) {
        this.day = day;
    }

    public BigDecimal getWeek() {
        return week;
    }

    public void setWeek(BigDecimal week) {
        this.week = week;
    }

    public BigDecimal getMonth() {
        return month;
    }

    public void setMonth(BigDecimal month) {
        this.month = month;
    }

    public Price() {
    }
}
