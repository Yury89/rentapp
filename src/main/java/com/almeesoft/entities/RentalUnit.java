package com.almeesoft.entities;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "RENTAL_UNIT")
@NamedQueries({
        @NamedQuery(name = "getRentalUnitById", query = "from RentalUnit r where r.id = :id "),
        @NamedQuery(name = "getMainRentalUnits", query = "from RentalUnit r where r.parentRentalUnit = null order by -id"),
        @NamedQuery(name = "getChildrenRentalUnitsById", query = "from RentalUnit r where r.parentRentalUnit.id = :id order by -id"),
        @NamedQuery(name = "getAllRentalUnit", query = "select r  from RentalUnit r order by -id"),
        @NamedQuery(name = "RENTAL_UNIT.getUsersRentalUnit", query = "from RentalUnit where user.id=:id order by -id "),
        @NamedQuery(name = "RENTAL_UNIT.getUsersRentalUnitForManage", query = "from RentalUnit where id != :current_id order by -id "),
        @NamedQuery(name = "RENTAL_UNIT.getUsersPolicy",query = "select r.policies from RentalUnit r where r.id = :id"),
        @NamedQuery(name = "RENTAL_UNIT.setPolicyValue",query = "update RentalUnit set policies = :policy where id = :id"),
        @NamedQuery(name = "RENTAL_UNIT.update",query = "update RentalUnit set name = :name, title = :title, description = :description where id = :id")


}
)

public class RentalUnit extends BaseEntity {


    @Column(name = "name")
    private String name;

    @Column(name = "title")
    private String title;

    @Column(name = "description")
    private String description;

    @Column(name = "plicies")
    private String policies;

    @Column(name = "full_address")
    private String fullAddress;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private User user;

    @OneToOne(fetch = FetchType.LAZY, mappedBy = "rentalUnit", cascade = CascadeType.ALL)
    private Price price;

    @OneToOne(mappedBy = "rentalUnit", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
    private Address address;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rentalUnit", cascade = CascadeType.ALL)
    private List<RentalAmenity> rentalAmenities;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rentalUnit", cascade = CascadeType.ALL)
    private List<Image> images;

    @OneToMany(fetch = FetchType.LAZY, mappedBy = "rentalUnit",cascade = CascadeType.ALL)
    private List<RentalPolicy> rentalPolicies;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_parent", referencedColumnName = "id")
    private RentalUnit parentRentalUnit;


    @OneToMany(fetch = FetchType.LAZY, mappedBy = "parentRentalUnit")
    private List<RentalUnit> sunRentalUnits;



    @Column(name = "showable")
    private Boolean showAble;


    public RentalUnit(String name, String title, String description, List<Image> images, List<RentalPolicy> polices, RentalUnit parentRentalUnit, List<RentalAmenity> amenities) {
        this.name = name;
        this.title = title;
        this.description = description;
        this.images = images;
        this.rentalPolicies = polices;
        this.parentRentalUnit = parentRentalUnit;
        this.rentalAmenities = amenities;

    }

    public RentalUnit() {
    }

    @Override
    public int hashCode() {
        return super.getId().hashCode();
    }

    @Override
    public String toString() {
        return super.getId() + " " + name + " " + title + " " + description;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof RentalUnit && this.getId().equals(((RentalUnit) obj).getId());
    }


    public List<RentalUnit> getSunRentalUnits() {
        return sunRentalUnits;
    }

    public void setSunRentalUnits(List<RentalUnit> sunRentalUnits) {
        this.sunRentalUnits = sunRentalUnits;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getPolicies() {
        return policies;
    }

    public void setPolicies(String policies) {
        this.policies = policies;
    }

    public Price getPrice() {
        return price;
    }

    public void setPrice(Price price) {
        this.price = price;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<RentalAmenity> getRentalAmenities() {
        return rentalAmenities;
    }

    public void setRentalAmenities(List<RentalAmenity> rentalAmenities) {
        this.rentalAmenities = rentalAmenities;
    }

    public List<Image> getImages() {
        return images;
    }

    public void setImages(List<Image> images) {
        this.images = images;
    }

    public List<RentalPolicy> getRentalPolicies() {
        return rentalPolicies;
    }

    public void setRentalPolicies(List<RentalPolicy> rentalPolicies) {
        this.rentalPolicies = rentalPolicies;
    }

    public RentalUnit getParentRentalUnit() {
        return parentRentalUnit;
    }

    public void setParentRentalUnit(RentalUnit rentalUnit) {
        this.parentRentalUnit = rentalUnit;
    }

    public Boolean getShowAble() {
        return showAble;
    }

    public void setShowAble(Boolean showAble) {
        this.showAble = showAble;
    }


    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }
}