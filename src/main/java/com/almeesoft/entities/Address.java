package com.almeesoft.entities;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "ADDRESS")
@NamedQueries({
        @NamedQuery(name = "ADDRESS.getAddressesById",query = "from Address  where rentalUnit.user.id = :idUser and id = :idRentalUnit"),
        @NamedQuery(name = "ADDRESS.deleteAddressFromRentalUnit",query = "delete Address where id = :id"),
//        @NamedQuery(name = "ADDRESS.insertRentalId",query = "update Address set rentalUnit.i")
})
public class Address extends BaseEntity{

    @Column(name = "country")
    private String country;

    @Column(name = "region")
    private String region;

    @Column(name = "city")
    private String city;

    @Column(name = "street")
    private String street;

    @Column(name = "home_number")
    private String homeNumber;

    @Column(name = "room")
    private String room;

    @Column(name = "full_address")
    private String fullAddress;


    @Column(name = "zip_code")
    private String zipCode;


    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "address_rental")
    private RentalUnit rentalUnit;


    public String getFullAddress() {
        return fullAddress;
    }

    public void setFullAddress(String fullAddress) {
        this.fullAddress = fullAddress;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getCountry() {

        return country;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public String getRegion() {
        return region;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getHomeNumber() {
        return homeNumber;
    }

    public void setHomeNumber(String homeNumber) {
        this.homeNumber = homeNumber;
    }

    public String getRoom() {
        return room;
    }

    public void setRoom(String room) {
        this.room = room;
    }

    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public Address() {
    }

    public boolean isPresent(){
        return !(Objects.equals(country, "") && Objects.equals(fullAddress, "") && Objects.equals(zipCode, "") && Objects.equals(region, ""));
    }
    @Override
    public String toString() {
        return "Address{" +
                "country='" + country + '\'' +
                ", region='" + region + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", homeNumber='" + homeNumber + '\'' +
                ", room='" + room + '\'' +
                '}';
    }
}
