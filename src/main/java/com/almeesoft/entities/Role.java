package com.almeesoft.entities;

import javax.persistence.*;

@Entity
@Table(name = "ROLES")
public class Role  extends BaseEntity{

    @Column(name = "role")
    private String role;

    @ManyToOne
    @JoinColumn(name = "id_user")
    private User user;

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
