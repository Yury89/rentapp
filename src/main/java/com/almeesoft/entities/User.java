package com.almeesoft.entities;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

@Entity
@Table(name = "USERS")
@NamedQueries({
        @NamedQuery(name = "USER.GetUserByName", query = "from User where login = :login"),
        @NamedQuery(name = "USER.getAllUsers", query = "from User")
}
)
public class User extends BaseEntity implements UserDetails {

    @Column(name = "login")
    private String login;

    @Column(name = "password")
    private String password;

    @Column(name = "name")
    private String name;

    @Column(name = "surname")
    private String surname;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<RentalUnit> rentalUnits;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Amenities> amenities;

    @OneToMany(mappedBy = "user",fetch = FetchType.LAZY)
    private List<Policy> policies;

    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    private List<Role> roles;

    public List<RentalUnit> getRentalUnits() {
        return rentalUnits;
    }

    public void setRentalUnits(List<RentalUnit> rentalUnits) {
        this.rentalUnits = rentalUnits;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public List<Policy> getPolicies() {
        return policies;
    }

    public void setPolicies(List<Policy> policies) {
        this.policies = policies;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public List<Amenities> getAmenities() {
        return amenities;
    }

    public void setAmenities(List<Amenities> amenities) {
        this.amenities = amenities;
    }

    public List<Role> getRoles() {
        return roles;
    }

    public void setRoles(List<Role> roles) {
        this.roles = roles;
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {

        List<SimpleGrantedAuthority> simpleGrantedAuthorities = new ArrayList<SimpleGrantedAuthority>();

        for (Role role1 : roles) {
            String prefix = "ROLE_";
            simpleGrantedAuthorities.add(new SimpleGrantedAuthority(prefix + role1.getRole()));
        }
        return simpleGrantedAuthorities;
    }

    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return login;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public User() {

    }


    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
