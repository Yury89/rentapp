package com.almeesoft.entities;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

//select CAST(COUNT(*) AS BIT) FROM [User] WHERE (UserID = 20070022)
@Entity
@Table(name = "AMENITIES")
@NamedQueries(
        {
                @NamedQuery(name = "getAllAmenities", query = "from Amenities order by -id"),
                @NamedQuery(name = "getAmenitiesById", query = "from Amenities  where id = :id "),
                @NamedQuery(name = "AMENITY.getGlobalAmenities",query = "from Amenities  where user = null order by -id"),
                @NamedQuery(name = "AMENITY.delete", query = "delete Amenities where id=:id "),
                @NamedQuery(name = "AMENITY.update",query = "update Amenities  set title=:title where id = :id"),
                @NamedQuery(name = "AMENITY.getUsersAmenity",query = "from Amenities where user.id = :id"),
                @NamedQuery(name = "AMENITY.updateDescription",query = "update Amenities set description=:description where id=:id"),
//                @NamedQuery(name = "AMENITY.isChecked", query = "select cast(count (*) as BOOLEAN ) from Amenities where id = :id"),
                @NamedQuery(name = "AMENITY.getPrice",query = "select r.price from RentalAmenity r where r.amenity.id = :amenityId and r.rentalUnit.id = :rentalId")

        }
)

public class Amenities extends BaseEntity {

    @Column(name = "title")
    private String title;


    @Column(name = "description")
    private String description;
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "amenity")
    private List<RentalAmenity> rentalAmenities;



    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_user")
    private User user;

    @Transient
    private Boolean isChecked = false;

    @Transient
    private BigDecimal price;

    @Transient
    private Boolean isParent = false;

    public Amenities(String title, String description, List<RentalAmenity> rentalAmenities) {

        this.title = title;
        this.description = description;
        this.rentalAmenities = rentalAmenities;
    }

    public Amenities() {
    }

    public Boolean getParent() {
        return isParent;
    }

    public void setParent(Boolean parent) {
        isParent = parent;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<RentalAmenity> getRentalAmenities() {
        return rentalAmenities;
    }

    public void setRentalAmenities(List<RentalAmenity> rentalAmenities) {
        this.rentalAmenities = rentalAmenities;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    @Override
    public boolean equals(Object o) {
        if(o == null)
            return false;
        if(o instanceof Amenities){
            Amenities a = (Amenities) o;
            return this.getTitle().equals(a.getTitle());
        }
        else return false;
    }

    @Override
    public int hashCode() {
        return getTitle().hashCode();
    }
}
