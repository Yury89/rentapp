package com.almeesoft.entities;

import javax.persistence.*;
import java.io.File;

@Entity
@Table(name = "IMAGE")
@NamedQueries({
        @NamedQuery(name = "IMAGE.deleteById", query = "delete Image  where id = :id")
})
public class Image extends BaseEntity {

    @Transient
    private File files;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "image_rental")
    private RentalUnit rentalUnit;


    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;

    }

    public Image() {

    }

    public Image(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public File getMultipartFile() {
        return files;
    }

    public void setMultipartFile(File files) {
        this.files = files;
    }

    public Image(File multipartFile,RentalUnit rentalUnit) {
        this.files = multipartFile;
        this.rentalUnit = rentalUnit;
    }
}
