package com.almeesoft.entities;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "POLICES")
@NamedQueries(
        {
                @NamedQuery(name = "getAllPolices", query = "from Policy order by -id"),
                @NamedQuery(name = "getPoliceById", query = "from  Policy  where  id = :id"),
                @NamedQuery(name = "POLICY.delete", query = "delete Policy where id=:id"),
                @NamedQuery(name = "POLICY.update",query = "update Policy set name = :name where id=:id"),
                @NamedQuery(name = "POLICY.getGlobalPolicy",query = "from Policy where user = null "),
                @NamedQuery(name = "POLICY,getUsersPolicy",query = "from Policy where user.id = :id"),
                @NamedQuery(name = "POLICY.updateDescription",query = "update Policy set description=:description where id=:id"),
                @NamedQuery(name = "POLICY.check", query = "select case when (count(r) > 0)  then true else false end from RentalPolicy r where r.rentalUnit.id = :idRental and r.policy.id = :idPolicy ")


        }
)

public class Policy extends BaseEntity {


    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "user_id")
    private User user;

    @OneToMany(mappedBy = "policy")
    private List<RentalPolicy> rentalPolicies;


    @Transient
    private Boolean isChecked = false;


    @Transient
    private Boolean isParent = false;



    public Policy() {

    }

    public Policy(String name, String description, List<RentalPolicy> rentalPolicies) {

        this.name = name;
        this.description = description;
        this.rentalPolicies = rentalPolicies;
    }


    public Boolean getParent() {
        return isParent;
    }

    public void setParent(Boolean parent) {
        isParent = parent;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public List<RentalPolicy> getRentalPolicies() {
        return rentalPolicies;
    }

    public void setRentalPolicies(List<RentalPolicy> rentalPolicies) {
        this.rentalPolicies = rentalPolicies;
    }


    @Override
    public boolean equals(Object o) {
        if (o == null)
            return false;
        if (o instanceof Policy) {
            Policy p = (Policy) o;
            if(p.name.equals(this.name))
                return true;
        }
        return false;
    }

    @Override
    public int hashCode() {
        return this.name.hashCode();
    }
}
