package com.almeesoft.entities;

import javax.persistence.*;

@Entity
@Table(name = "RENTAL_POLICES")
@NamedQueries(
        {
                @NamedQuery(query = "from  RentalPolicy", name = "getAllRentalPolices"),
                @NamedQuery(name = "RENTAL_POLICY",query = "delete RentalPolicy where policy.id = :id"),
                @NamedQuery(name = "RENTAL_POLICY.getCheckedPolicyById",query = "from RentalPolicy where rentalUnit.id = :id"),
                @NamedQuery(name = "RENTAL_POLICY.check",query = "select case when (count(r) > 0)  then true else false end " +
                        "from RentalPolicy r where r.rentalUnit.id = :idRental and r.policy.id = :idPol"),
                @NamedQuery(name = "RENTAL_POLICY.delete",query = "delete RentalPolicy where id = :id"),
                @NamedQuery(name = "RENTAL_POLICY.deleteAll",query = "delete RentalPolicy r where r.rentalUnit.id = :id")
        }
)
public class RentalPolicy extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "rental_id")
    private RentalUnit rentalUnit;

    @ManyToOne
    @JoinColumn(name = "polices_id")
    private Policy policy;

    public RentalPolicy() {
    }

    public RentalPolicy(RentalUnit rentalUnit, Policy policy) {
        this.rentalUnit = rentalUnit;
        this.policy = policy;
    }

    public RentalUnit getRentalUnit() {

        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public Policy getPolicy() {
        return policy;
    }

    public void setPolicy(Policy policy) {
        this.policy = policy;
    }

}
