package com.almeesoft.entities;

import javax.persistence.*;
import java.math.BigDecimal;

@Entity
@Table(name = "RENTAL_AMENITIES")
@NamedQueries({
        @NamedQuery(query = "from RentalAmenity" , name = "getAllRentalAmenity"),
        @NamedQuery(name = "RENTALAMENITY.delete",query = "delete RentalAmenity  where amenity.id=:id"),
        @NamedQuery(name = "RENTAL_AMENITY.getAmenitiesFromRentalUnit",query = " from RentalAmenity  where rentalUnit.id" +
                                                                                    " = :rentalId and rentalUnit.user.id = :userId"),
        @NamedQuery(name = "RENTAL_AMENITY.check",query = "select case when (count(r) > 0)  then true else false end " +
                                                          "from RentalAmenity r where r.rentalUnit.id = :idRental and r.amenity.id = :idAmen"),
        @NamedQuery(name = "RENTAL_AMEN.update",query = "update RentalAmenity set price = :price where id = :id"),
        @NamedQuery(name = "RENTAL_AMEN.delete", query = "delete RentalAmenity where id = :id"),
        @NamedQuery(name = "RENTAL_AMENITY.deleteAll",query = "delete RentalAmenity r where r.rentalUnit.id = :id")

})
public class RentalAmenity extends BaseEntity{

    @ManyToOne
    @JoinColumn(name = "amenities_id")
    private Amenities amenity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rental_id")
    private RentalUnit rentalUnit;

    @Column(name = "price")
    private BigDecimal price;

    @Transient
    private Boolean isChecked;

    public RentalAmenity( RentalUnit rentalUnit,Amenities amenity) {
        this.amenity = amenity;
        this.rentalUnit = rentalUnit;
    }

    public RentalAmenity() {
    }

    public Amenities getAmenity() {

        return amenity;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public void setAmenity(Amenities amenity) {
        this.amenity = amenity;
    }

    public RentalUnit getRentalUnit() {
        return rentalUnit;
    }

    public void setRentalUnit(RentalUnit rentalUnit) {
        this.rentalUnit = rentalUnit;
    }

    public Boolean getChecked() {
        return isChecked;
    }

    public void setChecked(Boolean checked) {
        isChecked = checked;
    }
}
