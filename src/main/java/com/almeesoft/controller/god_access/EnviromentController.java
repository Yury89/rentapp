package com.almeesoft.controller.god_access;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import com.almeesoft.envviroment_structure_mongodb.EnviromentStructure;

@Controller
@RequestMapping(value = "/env")
public class EnviromentController {
    @Autowired
    private EnviromentStructure enviromentStructure;


    @RequestMapping(method = RequestMethod.GET)
    public String enviroment(Model model){
        model.addAttribute("env",enviromentStructure);

        return "env";
    }
}
