package com.almeesoft.controller.anonimus_access;

import com.almeesoft.entities.RentalUnit;
import com.almeesoft.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/")
public class IndexController {


    @Autowired
    private RentalUnitService rentalUnitService;


    @RequestMapping
    public String mainRentalUnits(Model model) {
        for (RentalUnit rentalUnit : rentalUnitService.getAllRentalUnit()) {
            System.out.println(rentalUnit.getShowAble());
        }
        model.addAttribute("mainRentalUnits", rentalUnitService.getMainRentalUnits());
        return "index";
    }

}
