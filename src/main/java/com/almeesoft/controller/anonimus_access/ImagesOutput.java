package com.almeesoft.controller.anonimus_access;

import com.almeesoft.service.ImageService;
import com.mongodb.gridfs.GridFSDBFile;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
@RequestMapping("/uploaded_image")
public class ImagesOutput {


    @Autowired
    private ImageService imageService;


    @RequestMapping
    public String getRentalUnitsById(HttpServletResponse response, @RequestParam(value = "name") String name) {
        ServletOutputStream outputStream;
        try {
            outputStream = response.getOutputStream();

            GridFSDBFile image = imageService.getImage(name);
            IOUtils.copy(image.getInputStream(),outputStream);
            response.setContentType("image/jpeg");

            IOUtils.closeQuietly(outputStream);
            response.flushBuffer();
        } catch (IOException e) {
            e.printStackTrace();

        }
        return "rentalUnits";

    }
}
