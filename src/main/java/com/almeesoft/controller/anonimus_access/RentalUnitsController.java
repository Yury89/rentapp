package com.almeesoft.controller.anonimus_access;

import com.almeesoft.entities.RentalUnit;
import com.almeesoft.repository.AmenityRepository;
import com.almeesoft.repository.PoliceRepository;
import com.almeesoft.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;
import java.util.logging.Logger;

@Controller
@RequestMapping(value = "/rentalunit")
public class RentalUnitsController {

    private Logger l = Logger.getLogger(this.getClass().getCanonicalName());

    @Autowired
    private RentalUnitService rentalUnitService;

    @RequestMapping
    public String getRentalUnitsById(Model model, @RequestParam(value = "id") int id) {

        List<RentalUnit> parentRentalUnitsById = rentalUnitService.getParentRentalUnitsById(id);
        RentalUnit byId = rentalUnitService.getById(id);
        System.out.println(rentalUnitService.getChildrenRentalUnitsById(id));
        System.out.println(rentalUnitService.getById(id).getParentRentalUnit());
        model.addAttribute("child", rentalUnitService.getChildrenRentalUnitsById(id));
        model.addAttribute("parent", rentalUnitService.getById(id).getParentRentalUnit());
        model.addAttribute("current", byId);
        model.addAttribute("addresses",rentalUnitService.getFullAddresses(byId));
        model.addAttribute("mainAddress",rentalUnitService.getMainAddress((long) id));
        model.addAttribute("amenities",rentalUnitService.getAmenitiesForView(byId));
        model.addAttribute("polices",rentalUnitService.getPoliciesForView(byId));

        return "rentalUnits";

    }
}
