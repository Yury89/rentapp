package com.almeesoft.controller.administration;


import com.almeesoft.repository.security.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("administration/listOfUsers")
public class ListOfUsers {

    @Autowired
    private UserRepository userRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String listOfUsers(Model model){
        model.addAttribute("users",userRepository.getAllUsers());
        return "manage/lIstUser";
    }
}
