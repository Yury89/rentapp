package com.almeesoft.controller.administration;

import com.almeesoft.service.mysqldump.MysqlDumpService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("administration/mysql")
public class MysqlDumpController {

    @Autowired
    private MysqlDumpService mysqlDumpService;

    @RequestMapping(name = "/dump",method = RequestMethod.GET)
    public String dumping(){
        mysqlDumpService.backUp();
        return "administration/mysql";
    }


}
