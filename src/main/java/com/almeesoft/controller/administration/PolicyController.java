package com.almeesoft.controller.administration;

import com.almeesoft.entities.Amenities;
import com.almeesoft.entities.Policy;
import com.almeesoft.repository.PoliceRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("administration/addPolicy")
public class PolicyController {

    @Autowired
    private PoliceRepository policeRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String amenityForm(Model model){
        model.addAttribute("amenity",new Amenities());
        model.addAttribute("urlForAjax","administration");
        model.addAttribute("globalAmenities",policeRepository.getGlobalPolicy());
        return "manage/addPolicy";
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody String save(@RequestParam("value")String value,@RequestParam("desc") String desc){
        Policy amenities = new Policy();
        amenities.setName(value);
        amenities.setDescription(desc);
        policeRepository.save(amenities);
        return amenities.getId().toString();
    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    public @ResponseBody String ajaxDelete(@RequestParam("id") Long id){
        policeRepository.deleteAmenity(id);
        return "ok";
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public @ResponseBody String ajaxUpdate(@RequestParam("id") Long id,@RequestParam("value") String value){
        int res =  policeRepository.updateAmenity(id,value);
        return "ok";
    }

    @RequestMapping(value = "/updateDescription",method = RequestMethod.POST)
    public @ResponseBody String ajaxUpdateDescription(@RequestParam("id") Long id,@RequestParam("value") String val){
        policeRepository.updatePolicyDescription(id,val);
        return "ok";
    }

}
