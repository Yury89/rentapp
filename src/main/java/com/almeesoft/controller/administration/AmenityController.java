package com.almeesoft.controller.administration;

import com.almeesoft.entities.Amenities;
import com.almeesoft.repository.AmenityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("administration/addAmenity")
public class AmenityController {

    @Autowired
    private AmenityRepository amenityRepository;

    @RequestMapping(method = RequestMethod.GET)
    public String amenityForm(Model model,@RequestParam(name = "id",required = false) Long id){
        model.addAttribute("amenity",new Amenities());
        model.addAttribute("urlForAjax","administration");
        if(id == null)
            model.addAttribute("globalAmenities",amenityRepository.getGlobalAmenities());
        else
            model.addAttribute("globalAmenities",amenityRepository.getUsersAmenities(id));
        return "manage/addAmenity";
    }

    @RequestMapping(method = RequestMethod.POST)
    public @ResponseBody String save(@RequestParam("value")String value,@RequestParam("desc") String desc){
        Amenities amenities = new Amenities();
        amenities.setTitle(value);
        amenities.setDescription(desc);
        amenityRepository.save(amenities);
        return amenities.getId().toString();
    }

    @RequestMapping(value = "/delete",method = RequestMethod.POST)
    public @ResponseBody String ajaxDelete(@RequestParam("id") Long id){
        amenityRepository.deleteAmenity(id);
        return "ok";
    }

    @RequestMapping(value = "/update",method = RequestMethod.POST)
    public @ResponseBody String ajaxUpdate(@RequestParam("id") Long id,@RequestParam("value") String value){
        int res =  amenityRepository.updateAmenity(id,value);
        return "ok";
    }

    @RequestMapping(value = "/updateDescription",method = RequestMethod.POST)
    public @ResponseBody String ajaxUpdateDescription(@RequestParam("id") Long id,@RequestParam("value") String val){
            amenityRepository.updateAmenityDescription(id,val);
        return "ok";
    }

}
