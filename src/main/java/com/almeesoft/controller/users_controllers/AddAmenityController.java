package com.almeesoft.controller.users_controllers;

import com.almeesoft.entities.Amenities;
import com.almeesoft.entities.User;
import com.almeesoft.repository.AmenityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("manage/addAmenity")
public class AddAmenityController {

    @Autowired
    private AmenityRepository amenityRepository;

    private static String path = "dwwddw";

    @RequestMapping(method = RequestMethod.GET)
    public String amenityForm(Model model) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("amenity", new Amenities());
        model.addAttribute("urlForAjax","manage");
        model.addAttribute("globalAmenities", amenityRepository.getUsersAmenities(user.getId()));
        return "manage/addAmenity";
    }

    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String save(@RequestParam("value") String value,@RequestParam("desc") String desc) {
        Amenities amenities = new Amenities();
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        amenities.setTitle(value);
        amenities.setUser(user);
        amenities.setDescription(desc);
        amenityRepository.save(amenities);
        return amenities.getId().toString();
    }

    @RequestMapping(value = "/delete", method = RequestMethod.POST)
    public
    @ResponseBody
    String ajaxDelete(@RequestParam("id") Long id) {
        amenityRepository.deleteAmenity(id);
        return "ok";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public
    @ResponseBody
    String ajaxUpdate(@RequestParam("id") Long id, @RequestParam("value") String value) {
        int res = amenityRepository.updateAmenity(id, value);
        return "ok";
    }


    @RequestMapping(value = "/updateDescription", method = RequestMethod.POST)
    public @ResponseBody String updateDescription(@RequestParam("id") Long id,@RequestParam("value") String value){

        amenityRepository.updateAmenityDescription(id,value);
        return "ok";
    }

}
