package com.almeesoft.controller.users_controllers;

import com.almeesoft.entities.User;
import com.almeesoft.service.RentalUnitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/manage/manageRentalUnits")
public class ManageRentalUnit {

    @Autowired
    private RentalUnitService rentalUnitService;



    @RequestMapping(method = RequestMethod.GET)
    public String listOfRentalUnit(Model model){
        User principal = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        model.addAttribute("rentalUnits",rentalUnitService.getUsersRentalUnit(principal.getId()));
        return "manage_rental_unit/rentalList";
    }

}
