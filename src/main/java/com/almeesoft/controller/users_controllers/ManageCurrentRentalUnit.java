package com.almeesoft.controller.users_controllers;

import com.almeesoft.entities.*;
import com.almeesoft.repository.AmenityRepository;
import com.almeesoft.repository.PoliceRepository;
import com.almeesoft.service.RentalUnitService;
import com.almeesoft.service.manage_rental_unit.AmenityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.*;

@Controller
@RequestMapping("manage/rentalUnit/manageCurrentRentalUnit")
public class ManageCurrentRentalUnit {

    private final RentalUnitService rentalUnitService;

    private final AmenityService amenityService;

    private final PoliceRepository policeRepository;

    private final AmenityRepository amenityRepository;

    @Autowired
    public ManageCurrentRentalUnit(AmenityRepository amenityRepository, AmenityService amenityService, PoliceRepository policeRepository, RentalUnitService rentalUnitService) {
        this.amenityRepository = amenityRepository;
        this.amenityService = amenityService;
        this.policeRepository = policeRepository;
        this.rentalUnitService = rentalUnitService;
    }


    @RequestMapping
    public String show(Model model, @RequestParam(required = false) Long id) {
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (id == null) {
            model.addAttribute("rentalUnit", new RentalUnit());
            model.addAttribute("allRentalUnits", rentalUnitService.getUsersRentalUnit(user.getId()));
            model.addAttribute("amenities", amenityRepository.getGlobalAmenities());
            model.addAttribute("policies", policeRepository.getGlobalPolicy());
        } else {
            RentalUnit byId = rentalUnitService.getById(id);
            model.addAttribute("rentalUnit", byId);
            model.addAttribute("allRentalUnits", rentalUnitService.getRentalUnitForManage(id));
            model.addAttribute("address",rentalUnitService.getMainAddress(id));
            model.addAttribute("amenities", amenityService.getAmenityForManage(user.getId(), id));
            model.addAttribute("policies", policeRepository.getPolicyForManage(id));
        }




        return "manage_rental_unit/manageRentalUnit";
    }

    @RequestMapping(method = RequestMethod.POST)
    public
    @ResponseBody
    String update(@ModelAttribute RentalUnit rentalUnit,
                  MultipartHttpServletRequest request,
                  @RequestParam(value = "deleteIdImage[]", required = false) Long[] ids) throws IOException {
//                                           if(rentalUnit.getRentalAmenities() != null)
//        for (RentalAmenity rentalAmenity : rentalUnit.getRentalAmenities()) {
//            System.out.println(rentalAmenity.getAmenity().getId());
//        }
        if(rentalUnit.getParentRentalUnit().getId() == -1)
            rentalUnit.setParentRentalUnit(null);
        if(rentalUnit.getParentRentalUnit() != null)
            rentalUnit.setAddress(null);
        Iterator<String> fil = request.getFileNames();
        List<MultipartFile> files = new ArrayList<MultipartFile>();
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        rentalUnit.setUser(user);
        while (fil.hasNext()) {

            files.add(request.getFile(fil.next()));

        }
        if (rentalUnit.getId() == null)
            rentalUnitService.saveImageAndRentalUnit(rentalUnit, files);
        else
            rentalUnitService.updateRentalUnitforManage(rentalUnit, ids, files);
        return String.valueOf(rentalUnit.getId());
    }

    @ResponseBody
    @RequestMapping(value = "/getData", method = RequestMethod.POST)
    public Map<String,Object> getData(@RequestBody Long[] datas) {
        Address address;
        if(datas[1] != -1)
         address = rentalUnitService.getMainAddress((datas[1]));
        else address = rentalUnitService.getMainAddress(datas[0]);
        User user = (User) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        List<Amenities> amenityForManage;
        List<Policy> policies;
        if(datas[0] != -1) {
            if(datas[1] != -1) {
                amenityForManage = amenityService.getAmenityForManage(user.getId(), datas[1]);
                policies = policeRepository.getPolicyForManage(datas[1]);
            }
            else {
                amenityForManage = amenityRepository.getGlobalAmenities();
                policies = policeRepository.getGlobalPolicy();
            }
        }
        else {
            amenityForManage = amenityRepository.getGlobalAmenities();
            policies = policeRepository.getGlobalPolicy();
        }
        HashMap<String, Object> stringStringHashMap = new HashMap<String,Object>();

        if(address != null) {
            stringStringHashMap.put("country", address.getCountry());
            stringStringHashMap.put("region", address.getRegion());
            stringStringHashMap.put("zupCode", address.getZipCode());
        }
        String[] amenity = new String [amenityForManage.size()];
        Boolean[] parent = new Boolean [amenityForManage.size()];
        Boolean[] checked = new Boolean [amenityForManage.size()];
        for (int i = 0; i < amenityForManage.size(); i++){
            amenity[i] = amenityForManage.get(i).getTitle();
            parent[i] = amenityForManage.get(i).getParent();
            checked[i] = amenityForManage.get(i).getChecked();
        }
        Long[] idAmenity = new Long [amenityForManage.size()];
        BigDecimal[] prices = new BigDecimal [amenityForManage.size()];
        for (int i = 0; i < amenityForManage.size(); i++){
            prices[i] = amenityForManage.get(i).getPrice();
            idAmenity[i] = amenityForManage.get(i).getId();
        }
        stringStringHashMap.put("parent",parent);
        stringStringHashMap.put("check",checked);
        stringStringHashMap.put("amenities_id",idAmenity);
        stringStringHashMap.put("amenities",amenity);
        stringStringHashMap.put("prices",prices);
        Long[] longs = new Long[policies.size()];
        for (int i = 0; i < longs.length; i++) {
             longs[i] = policies.get(i).getId();
        }
        stringStringHashMap.put("policies",longs);

        return stringStringHashMap;
    }
}
