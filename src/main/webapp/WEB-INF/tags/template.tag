<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <title>Rent Houses</title>
    <link  href="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.css" rel="stylesheet"> <!-- 3 KB -->
    <link href="<c:url value="/resources/css/foundation.css"/>" rel="stylesheet">
    <link href="<c:url value="/resources/css/styles.css"/>" rel="stylesheet">

</head>
<body>
<div class="top-bar">
    <div class="top-bar-left">
        <ul class="menu">
            <li class="menut"><a href="<c:url value="/"/>"> Rent Optimum</a></li>
        </ul>
    </div>
    <div class="top-bar-right">
        <ul class="menu dropdown" data-dropdown-menu>
            <li>
                <a href="#">Language</a>
                <ul class="menu vertical">
                    <li><a href="#">English</a></li>
                    <li><a href="#">Chinese</a></li>
                    <li><a href="#">Spanish</a></li>
                    <li><a href="#">Russian</a></li>
                </ul>
            </li>
            <sec:authorize access="hasRole('USER')">

                <li>
                    <a href="#">Manage Rental Units</a>
                    <ul class="menu vertical">
                        <li><a href="<c:url value="/manage/rentalUnit/manageCurrentRentalUnit"/>">Add Rental Unit</a>
                        </li>
                        <li><a href="<c:url value="/manage/manageRentalUnits"/>">Manage Rental Units</a></li>
                        <li><a href="<c:url value="/manage/addAmenity"/>">Manage My Amenities</a></li>
                    </ul>
                </li>

            </sec:authorize>
            <sec:authorize access="hasRole('ADMIN')">

                <li>
                    <a href="#">Administration Retal Units</a>
                    <ul class="menu vertical">
                        <li><a href="<c:url value="/administration/addAmenity"/>">Manage Global Amenities</a></li>
                        <li><a href="<c:url value="/administration/addPolicy"/>">Manage Global Policies</a></li>
                        <li><a href="<c:url value="/administration/listOfUsers"/>">List of Users</a></li>
                    </ul>
                </li>


            </sec:authorize>
            <c:if test="${pageContext.request.userPrincipal.name != null}">
                <li><a href="<c:url value="/j_spring_security_logout"/>">Logout</a></li>
            </c:if>
            <c:if test="${pageContext.request.userPrincipal.name == null}">
                <li><a href="<c:url value="/login"/>">Login</a></li>
            </c:if>
        </ul>
    </div>
</div>
<br/>
<div>

    <jsp:doBody/>

</div>
<script src="<c:url value="/resources/js/foundation.js"/>"></script>
<script>$(document).foundation();</script>
</body>
</html>