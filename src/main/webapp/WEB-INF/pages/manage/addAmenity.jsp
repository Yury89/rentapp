<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:template>
    <c:if test="${request.getParameter('id') == null}">
    <div class="row">
        <div class="col l3">
            <label for="addNewAmenity">Name</label>
            <input type="text" id="addNewAmenity">
            <button onclick="addNewAmenity()" class="button">Add New Amenity</button>
        </div>
    </div>
    </c:if>
    <div class="row">
        <div class="col offset-l3 l6">
            <table class="centered">
                <thead>
                <tr>
                    <th data-field="id">Id</th>
                    <th data-field="name">Amenity Name</th>
                    <th data-field="action_delete">Delete</th>
                </tr>
                </thead>

                <tbody id="insertNewAmenity">
                <c:forEach items="${globalAmenities}" var="globalAmenity">
                    <tr id="amenity${globalAmenity.id}">
                        <td id="beforeTitle${globalAmenity.id}">${globalAmenity.id}</td>
                        <td id="title${globalAmenity.id}">${globalAmenity.title}</td>
                        <td id="insertDescription${globalAmenity.id}"><input type="text" id="update${globalAmenity.id}">
                            <button class="button" onclick="updateAmenity(${globalAmenity.id})">Update Name</button>
                        </td>
                        <td>
                            <button class="button" onclick="deleteAmenity(${globalAmenity.id})">Delete</button>
                        </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>

    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
        <c:url value="/${urlForAjax}/addAmenity/delete" var="del"/>
        <c:url value="/${urlForAjax}/addAmenity/update" var="update"/>
        <c:url value="/${urlForAjax}/addAmenity?value=" var="newAction"/>

        function deleteAmenity(id) {
            $.ajax({
                type: "POST",
                url: "${del}?id=".concat(id),
                success: deleteAmenityById(id)
            })
        }

        function deleteAmenityById(id) {
            var el = "#amenity".concat(id);
            $(el).remove();
        }

        function updateAmenity(id) {
            var input = "#update".concat(id);
            var value = $(input).val();

            $.ajax(
                    {
                        type: "POST",
                        url: "${update}?id=".concat(id).concat("&value=").concat(value),
                        success: addAmenity(value,id)
                    }
            )
        }

        function addAmenity(value,id) {
            var el = "#title".concat(id);
            $(el).remove();
            var afterEl = "#beforeTitle".concat(id);
            $(afterEl).after(function () {
                return "<td id='title" + id + "'>" + value + "</td>"
            })

        }

        function addNewAmenity() {
            var val = $("#addNewAmenity").val();
            var desc = $("#addNewAmenityDesc").val();
            $.ajax({
                type: "POST",
                url: "${newAction}".concat(val).concat("&desc=").concat(desc),
                success:function (data) {
                    addNewAmenityTable(data,val,desc);
                }

            })
        }

        function addNewAmenityTable(id,val,desc) {
                $("#insertNewAmenity").after(function () {
                    return   "<tr id='amenity"+ id +"'>" +
                            "<td id='beforeTitle"+ id +"'>"+ id +"</td>" +
                            "<td id='title"+ id +"'>"+ val +"</td>" +
                            "<td id='insertDescription"+ id +"'><input type='text' id='update"+ id +"'>" +
                            "<button class='button' onclick='updateAmenity("+ id +")'>Update Name</button>" +
                    "</td> <td><button class='button' onclick='deleteAmenity("+ id +")'>Delete</button> </td></tr>";
                })
        }

    </script>
</t:template>