<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:template>
    <div class="row">
        <div class="col offset-l3 l6">
            <table class="centered">
                <thead>
                <tr>
                    <th data-field="id">Id</th>
                    <th data-field="name">User Name</th>
                    <th data-field="update">User login</th>
                    <th data-field="action_see">List of Amenity</th>
                </tr>
                </thead>

                <tbody id="insertNewAmenity">
                <c:forEach items="${users}" var="user">
                    <tr>
                    <td>${user.id}</td>
                    <td>${user.name}</td>
                    <td>${user.login}</td>
                    <td><a href="<c:url value="/administration/addAmenity?id=${user.id}"/>">Amenities</a> </td>
                    </tr>
                </c:forEach>
                </tbody>
            </table>
        </div>
    </div>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
</t:template>