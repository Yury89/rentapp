<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<t:template>

    <div class="row  large-up-2">
        <c:forEach items="${mainRentalUnits}" var="rent">

            <div class="column">

                <c:if test="${rent.images.size() != 0}">
                    <a href="<c:url value="/rentalunit?id=${rent.id}"/>">
                        <img class="thumbnail"
                             src="<c:url  value="/uploaded_image?name=${rent.images.get(0).id}"/>" width="570"
                             height="470">
                    </a>
                </c:if>

                <c:if test="${rent.images.size() == 0}">
                    <img class="thumbnail"
                         src="<c:url  value="/resources/no-photo.png"/>" width="400" height="300">
                </c:if>

                <h5>${rent.name}</h5>


                <p><c:if test="${rent.price.day != null}">
                    $${rent.price.day}
                </c:if>

                </p>
                <a href="<c:url value="/rentalunit?id=${rent.id}"/>" class="button expanded">More</a>
            </div>

        </c:forEach>
    </div>

    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

</t:template>