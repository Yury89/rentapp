<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<t:template>
 

    <c:forEach items="${checkedPolicy}" var="rentalPolicy" varStatus="i">
        <div class="row" id="delete${rentalPolicy.id}">
            <div class="input-field col offset-l3 s6">

                <h3>${rentalPolicy.policy.name}</h3>
                <button onclick="delAmen(${rentalPolicy.id},${rentalPolicy.policy.id},'${rentalPolicy.policy.name}')"
                        type="button">Delete
                </button>
            </div>
        </div>
    </c:forEach>
    <c:url var="action" value="/manage/rental_unit/policy"/>
    <form:form cssClass="col s12" method="post" action="${action}" modelAttribute="rentalAmenity">

        <input type="hidden" value="${param.id}" name="idRental">
        <h1>Other Policy</h1>
        <c:forEach items="${notCheckedPolicy}" var="amenity" varStatus="status">
            <p>
                <input type="checkbox" id="amenity${amenity.id}" value="${amenity.id}" name="policy[]"
                       onclick="addPriceField(${amenity.id})"/>
                <label for="amenity${amenity.id}" id="labelAmenity${amenity.id}">${amenity.name}</label>
            </p>
        </c:forEach>
        <label for="usersPolicy">Your Policy</label>
        <input id="usersPolicy" type="text" name="usersPolicy" value="${usersPolicies}">

        <div id="add">

        </div>
        <button type="submit">Ok</button>

    </form:form>

    <script>

        function updatePrice(id) {
            var value = $("#update".concat(id)).val();

            $.ajax({
                type: "post",
                url: "/manage/rental_unit/policy/rentalPol?id=".concat(id).concat("&val=").concat(value)

            })
        }

        function delAmen(id, amenId, title) {
            $.ajax({
                type: "post",
                url: "/manage/rental_unit/policy/delPol?id=".concat(id),
                success: deleteAmen(id, amenId, title)
            })
        }

        function deleteAmen(id, amenId, title) {
            $("#delete".concat(id)).remove();
            $("#add").after(function () {
                return "<input type='checkbox' value='" + amenId + "' name='policy[]' id='inputAmenity" + amenId + "'>" +
                        "<label for='addAmen" + amenId + "'>" + title + "</label>";
            })
        }
    </script>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

</t:template>