<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<t:template>
  
    <c:forEach items="${rentalAmenities}" var="rentalAmenity">
        <div class="row" id="delete${rentalAmenity.id}">
            <div class="input-field col offset-l3 s6">
                <label for="update${rentalAmenity.id}">${rentalAmenity.amenity.title}</label>
                <input type="text" value="${rentalAmenity.price}" id="update${rentalAmenity.id}"
                       onchange="updatePrice(${rentalAmenity.id})">
                <button onclick="delAmen(${rentalAmenity.id},${rentalAmenity.amenity.id},'${rentalAmenity.amenity.title}')"
                        type="button">Delete
                </button>
            </div>
        </div>
    </c:forEach>
    <c:url var="action" value="/manage/rental_unit/amenities"/>
    <form:form cssClass="col s12" method="post" action="${action}" modelAttribute="rentalAmenity">

        <input type="hidden" value="${request.getParameter("id")}" name="idRental">
        <h1>Other Amenity</h1>
        <c:forEach items="${amenities}" var="amenity" varStatus="status">
            <p>
                <input type="checkbox" id="amenity${amenity.id}" value="${amenity.id}" name="amenity[]"
                       onclick="addPriceField(${amenity.id})"/>
                <label for="amenity${amenity.id}" id="labelAmenity${amenity.id}">${amenity.title}</label>
            </p>
        </c:forEach>
        <div id="add">

        </div>
        <button type="submit">Ok</button>

    </form:form>

    <script>

        function updatePrice(id) {
            var value = $("#update".concat(id)).val();

            $.ajax({
                type: "post",
                url: "/manage/rental_unit/amenities/rentalAmen?id=".concat(id).concat("&val=").concat(value)

            })
        }

        function delAmen(id, amenId, title) {
            $.ajax({
                type: "post",
                url: "/manage/rental_unit/amenities/delAmen?id=".concat(id),
                success: deleteAmen(id, amenId, title)
            })
        }

        function deleteAmen(id, amenId, title) {
            $("#delete".concat(id)).remove();
            $("#add").after(function () {
                return "<input type='checkbox' value=' " + amenId + "' name='amenity[]' id='inputAmenity" + amenId + "'>" +
                        "<label for='addAmen" + amenId + "'>" + title + "</label>";
            })
        }
    </script>
    <script src="<c:url value="/resources/js/addInputToAmenity.js"/>"></script>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

</t:template>