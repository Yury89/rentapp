<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>


<t:template>

    <table>
        <thead>
        <tr>
            <th data-field="id">Image</th>
            <th data-field="name">Name</th>
            <th data-field="title">Title</th>
            <th data-field="action">Edit</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach items="${rentalUnits}" var="rent">
        <tr>
            <td><c:if test="${rent.images.size() != 0}">
                <img class="activator cursor-pointer"
                     src="<c:url  value="/uploaded_image?name=${rent.images.get(0).id}"/> " width="100" height="150">
            </c:if>

                <c:if test="${rent.images.size() == 0}">
                    <img class="activator cursor-pointer"
                         src="<c:url  value="/resources/no-photo.png"/>" width="100" height="150">
                </c:if>
            </td>
            <td>${rent.name}</td>
            <td>${rent.title}</td>
            <td><a href="<c:url value="/manage/rentalUnit/manageCurrentRentalUnit?id=${rent.id}"/>">Edit</a></td>
        </tr>
        </c:forEach>
        </tbody>
    </table>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>

</t:template>