<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>


<t:template>

    <div class="row">
        <div class="columns">
            <h3>Choose Parent Rental Unit</h3>
            <c:forEach items="${allRentalUnits}" var="parent">
                <div class="thumbnail <c:if test="${parent.id == rentalUnit.parentRentalUnit.id}">ro_add_color</c:if> "
                     onclick="getDataFromServer(${parent.id})" id="parrent${parent.id}">

                    <c:if test="${parent.images.size() != 0}">
                        <img src="<c:url value="/uploaded_image?name=${parent.images.get(0).id}"/>" width="100"
                             height="50">
                    </c:if>
                    <c:if test="${parent.images.size() == 0}">
                        <img src="<c:url value="/resources/no-photo.png"/>" width="100" height="50">
                    </c:if>
                        ${parent.name}
                </div>
            </c:forEach>
        </div>
        <div class="input-field col s8">
            <div class="row">

                    <c:url var="action" value="/manage/rentalUnit/manageCurrentRentalUnit"/>
                <form:form cssClass="col s12" modelAttribute="rentalUnit"
                           enctype="multipart/form-data" name="main_form">
                    <sec:csrfInput/>
                <input type="hidden" value="${pageContext.request.getParameter("id")}" name="id">
                <div class="row">
                    <div class="input-field col s6">
                        <form:label path="name">Name
                            <form:input path="name" id="name" placeholder="Enter Name"/>
                            <div class="valName">
                            </div>
                        </form:label>


                    </div>
                </div>
                <div id="close">

                    <div class="row">
                        <div class="input-field col s12">
                            <form:label path="description">Description
                                <form:textarea path="description" id="description" placeholder="Enter Description"
                                               cssClass="materialize-textarea"/>
                                <div class="valName">

                                </div>
                            </form:label>
                        </div>
                    </div>
                    <input type="hidden" name="price.id" value="${rentalUnit.price.id}"/>
                    <div class="row">
                        <h3>Prices:</h3>
                        <div class="input-field col offset-l3 s6">
                            <form:label path="price.day">Price For Day
                                <form:input path="price.day" id="price_day" placeholder="Enter Price for Day"/>

                                <div class="valName">

                                </div>
                            </form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:label path="price.week">Price For Week
                                <form:input path="price.week" id="price_week" placeholder="Enter Price for Week"/>
                                <div class="valName">

                                </div>
                            </form:label>
                        </div>
                    </div>
                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:label path="price.month">Price For Month
                                <form:input path="price.month" id="price_month" placeholder="Enter Price for Month"/>
                                <div class="valName">

                                </div>
                            </form:label>
                        </div>
                    </div>
                    <div id="address"
                         class=" <c:if test="${rentalUnit.parentRentalUnit != null}">ro_display_none</c:if>">

                        <div class="row">
                            <h3>Address:</h3>
                            <div class="input-field col offset-l3 s6">
                                <input type="hidden" value="${rentalUnit.address.id}" name="address.id">
                                <form:label path="address.country">Address Country
                                    <form:input path="address.country" id="address_coutry"
                                                placeholder="Enter Address Country"/>
                                    <div class="valName">

                                    </div>
                                </form:label>
                            </div>
                        </div>
                        <div class="row ui-widget">
                            <div class="input-field col offset-l3 s6">
                                <form:label path="address.region">Address Region
                                    <form:input path="address.region" id="address_region"
                                                placeholder="Enter Address Region"/>
                                    <div class="valName">

                                    </div>
                                </form:label>
                            </div>
                        </div>
                        <div class="row">
                            <div class="input-field col offset-l3 s6">
                                <form:label path="address.zipCode">Enter Zip Code
                                    <form:input path="address.zipCode" id="address_zip_code"
                                                placeholder="Enter Zip Code"/>
                                    <div class="valName">

                                    </div>
                                </form:label>
                            </div>
                        </div>

                    </div>
                    <div id="parrent_address"
                         <c:if test="${rentalUnit.parentRentalUnit == null}">class="ro_display_none"</c:if>>
                        <div class="thumbnail">

                            <h3>Address</h3>
                            <c:if test="${address.country != ''}">
                                <p id="country_m">Country ${address.country}</p>
                            </c:if>
                            <c:if test="${address.region != ''}">
                                <p id="region_m">Region ${address.region}</p>
                            </c:if>

                            <c:if test="${address.zipCode != ''}">
                                <p id="zip_m">Zip Code ${address.zipCode}</p>
                            </c:if>

                        </div>
                    </div>

                    <div class="row">
                        <div class="input-field col offset-l3 s6">
                            <form:label path="fullAddress">Enter Address
                                <form:input path="fullAddress" id="fullAddress" placeholder="Enter Address"/>
                                <div class="valName">

                                </div>
                            </form:label>
                        </div>
                    </div>


                        <%--                                        PROPERTIES                                                                                  --%>
                    <div class="row">
                        <fieldset class="large-6 columns" id="amenities">
                            <legend><h3>Check Amenities</h3></legend>
                            <div id="insertAmenity">
                                <div id="delAMen">
                                    <c:forEach items="${amenities}" var="amenity" varStatus="status">
                                        <c:if test="${amenity.parent}">
                                            <h6>${amenity.title}
                                                <c:if test="${amenity.price!= null}">
                                                    ${amenity.price}$
                                                </c:if>
                                                <c:if test="${amenity.price == null}">
                                                    Free
                                                </c:if>
                                            </h6>
                                        </c:if>
                                        <c:if test="${!amenity.parent}">
                                            <label for="amenity${amenity.id}"
                                                   id="labelAmenity${amenity.id}">${amenity.title}
                                                <input type="checkbox" id="amenity${amenity.id}" value="${amenity.id}"
                                                       name="rentalAmenities[${status.index}].amenity.id"
                                                       onclick="addPriceField(${amenity.id},${status.index})"

                                                <c:if test="${amenity.checked}">
                                                       checked
                                                </c:if>

                                                >
                                            </label>
                                            <c:if test="${amenity.checked}">

                                                <input type="text" id="inputAmenity${amenity.id}"
                                                       value="${amenity.price}"
                                                       name="rentalAmenities[${status.index}].price"/>
                                            </c:if>
                                        </c:if>
                                    </c:forEach>
                                </div>
                            </div>
                        </fieldset>
                    </div>

                    <h3>Polices</h3>
                    <div id="policy">
                        <div id="insertPolicy">
                            <c:forEach items="${policies}" var="police" varStatus="status">
                                <c:if test="${police.parent}">
                                    <h6>${police.name}</h6>
                                </c:if>
                                <c:if test="${!police.parent}">


                                    <p>
                                        <label for="police${police.id}">${police.name}
                                            <input type="checkbox" id="police${police.id}" value="${police.id}"
                                                   name="rentalPolicies[${status.index}].policy.id"  <c:if
                                                    test="${police.checked}"> checked</c:if>/>
                                        </label>
                                    </p>
                                </c:if>
                            </c:forEach>
                        </div>
                        <div class="row">
                            <div class="input-field col offset-l3 s6">
                                <form:label path="policies">Enter Aditional Polices
                                    <form:input path="policies" id="description" placeholder="Enter Aditional Polices"/>
                                    <div class="valName">

                                    </div>
                                </form:label>
                            </div>
                        </div>
                    </div>

                        <%--                                        PROPERTIES                                                                                  --%>
                        <%--<h3>Images</h3>--%>

                        <%--<div class="row" id="addImageForm">--%>
                        <%--<div class="file-field input-field col offset-l3 l5">--%>
                        <%--<div class="btn">--%>
                        <%--<span>File 0</span>--%>
                        <%--<input name="myImages" type="file" multiple="multiple" id="imagesForUpload"/>--%>
                        <%--</div>--%>
                        <%--<div class="file-path-wrapper">--%>
                        <%--<input class="file-path validate" type="text" onchange="createPreciew()">--%>
                        <%--</div>--%>
                        <%--</div>--%>
                        <%--</div>--%>

                    <div id="drop_zone" class="border-for-drag-images">
                        <table>
                            <thead>
                            <tr>
                                <th data-field="mid">MainImage</th>
                                <th data-field="mname">delete</th>
                            </tr>

                            </thead>
                            <tbody id="mainInsertImage" ondrop="drop(event)" ondragover="allowDrop(event)">
                            <tr>
                                <td>Main Image drop here</td>
                                <td></td>
                            </tr>

                            </tbody>
                        </table>
                        <table>
                            <thead>
                            <tr>
                                <th data-field="id">Image</th>
                                <th data-field="name">delete</th>
                            </tr>
                            </thead>

                            <tbody id="insertImage">
                            <c:forEach items="${rentalUnit.images}" var="image">
                                <tr id="image${image.id}" ondragstart="drag(event)" draggable="true">
                                    <td><img src="<c:url value="/uploaded_image?name=${image.id}"/>" width="350"
                                             height="300"
                                             id="oldImage${image.id}" onclick="zoomOld(${image.id})" draggable="false"/>
                                    </td>
                                    <td>
                                        <button onclick="addToDelete(${image.id})" type="button">Delete</button>
                                    </td>

                                </tr>

                            </c:forEach>

                            </tbody>
                        </table>
                        <h1> Drop Here </h1>
                        <input type="file" id="files" multiple>
                        <output id="list"></output>
                    </div>


                    <button type="submit" class="button">Submit</button>

                    </form:form>

                    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
                    <script type="text/javascript" src="<c:url value="/resources/js/jquery.validate.min.js"/>"></script>
                    <script src="<c:url value="/resources/js/myValidation.js"/>"></script>
                    <script src="<c:url value="/resources/js/jquery.autocomplete.js"/>"></script>
                    <script src="<c:url value="/resources/js/addInputToAmenity.js"/>"></script>
                    <script src="<c:url value="/resources/js/manage.js"/>"></script>
                    <script>
                        var idMainElement = -1;
                        var removableEl;
                        function allowDrop(ev) {
                            ev.preventDefault();

                        }

                        function drag(ev) {
                            ev.dataTransfer.setData("text", ev.target.id);

                        }

                        function drop(ev) {
                            ev.preventDefault();
                            var data = ev.dataTransfer.getData("text");
                            if (idMainElement != -1) {
                                removableEl = $("#".concat(idMainElement));
                                $("#".concat(idMainElement)).remove();
                                $('#insertImage').append(removableEl);
                            }
                            $("#mainInsertImage").append($('#'.concat(data)));
                            idMainElement = document.getElementById(data).id;
                        }

                    </script>
                    <script>
                        var parentRentalUnit = -1;
                        var currentRental = -1;
                        <c:if test="${pageContext.request.getParameter('id') != null}">
                        currentRental = ${pageContext.request.getParameter("id")};
                        </c:if>
                        <c:if test="${rentalUnit.parentRentalUnit != null}">
                        parentRentalUnit = ${rentalUnit.parentRentalUnit.id};
                        </c:if>
                        $(document).ready(function () {

                            var availableTags = [
                                "Alabama",
                                "Alaska",
                                "Arizona",
                                "Arkansas",
                                "California",
                                "Colorado",
                                "Connecticut",
                                "Delaware",
                                "Florida",
                                "Georgia",
                                "Hawaii",
                                "Idaho",
                                "Illinois Indiana",
                                "Iowa",
                                "Kansas",
                                "Kentucky",
                                "Louisiana",
                                "Maine",
                                "Maryland",
                                "Massachusetts",
                                "Michigan",
                                "Minnesota",
                                "Mississippi",
                                "Missouri",
                                "Montana Nebraska",
                                "Nevada",
                                "New Hampshire",
                                "New Jersey",
                                "New Mexico",
                                "New York",
                                "North Carolina",
                                "North Dakota",
                                "Ohio",
                                "Oklahoma",
                                "Oregon",
                                "Pennsylvania Rhode Island",
                                "South Carolina",
                                "South Dakota",
                                "Tennessee",
                                "Texas",
                                "Utah",
                                "Vermont",
                                "Virginia",
                                "Washington",
                                "West Virginia",
                                "Wisconsin",
                                "Wyoming"];


                            $('#address_region').autocomplete({
                                lookup: availableTags

                            });

                        });

                        var allFiles = [];
                        var redirectId;
                        var deleteImageIds = [];
                        var current;

                        function handleFileSelect(evt) {
                            evt.stopPropagation();
                            evt.preventDefault();

                            var files = evt.target.files;
                            if (files == null)
                                files = evt.dataTransfer.files; // FileList object.
                            for (var i = 0, f; f = files[i]; i++) {
                                allFiles.push(f);
                                current = i;

                                // Only process image files.
                                if (!f.type.match('image.*')) {
                                    continue;
                                }

                                var reader = new FileReader();

                                // Closure to capture the file information.
                                reader.onload = (function (theFile) {
                                    return function (e) {
                                        // Render thumbnail.
//                            var span = document.createElement('span');
////                            span.innerHTML = ['<img class="thumb" src="', e.target.result,
////                                '" title="', escape(theFile.name), '"/>'].join('');
////                            document.getElementById('list').insertBefore(span, null);
//                            span.innerHTML = '<tr id="new_image' + current +'">' +
//                                    '<td><img src="' + e.target.result + '" width="300" height="300" /> </td>' +
//                                   '<td><button  onclick="newDelete(' + current+ ')" type="button">Delete</button></td></tr>';
//                            $("#insertImage").insertBefore(span);
                                        $("#insertImage").append(
                                                '<tr id="new_image' + current + '" ondragstart="drag(event)" draggable="true" >' +
                                                '<td><img   src="' + e.target.result + '" width="350" height="300" id="newImage' + current + '" onclick="zoomCurrent(' + current + ')" /> </td>' +
                                                '<td><button  onclick="newDelete(' + current + ')" type="button">Delete</button></td></tr>'
                                        )


                                    };
                                })(f);

                                // Read in the image file as a data URL.
                                reader.readAsDataURL(f);
                            }
                        }
                        function newDelete(id) {
                            allFiles.splice(1, id);
                            console.log(id);
                            $("#new_image".concat(id)).remove();

                        }
                        function handleDragOver(evt) {
                            evt.stopPropagation();
                            evt.preventDefault();
                            evt.dataTransfer.dropEffect = 'copy'; // Explicitly show this is a copy.
                        }

                        var dropZone = document.getElementById('drop_zone');
                        dropZone.addEventListener('dragover', handleDragOver, false);
                        dropZone.addEventListener('drop', handleFileSelect, false);
                        document.getElementById('files').addEventListener('change', handleFileSelect, false);

                        <c:url var="redirect" value="/rentalunit?id="/>

                        form = document.forms.namedItem("main_form");
                        form.addEventListener('submit', function (ev) {
                            var oData = new FormData(form);
                            for (var j = 0; j < allFiles.length; j++) {
                                oData.append("mfiles[".concat(j.toString()).concat("]"), allFiles[j]);
                            }
                            oData.append("deleteIdImage[]", deleteImageIds);
                            oData.append("parentRentalUnit.id", parentRentalUnit);
                            var oReq = new XMLHttpRequest();

                            oReq.open("POST", "${action}", true);
                            oReq.onload = function (oEvent) {
                                if (oReq.status == 200) {
                                    window.location.replace("${redirect}".concat(oReq.responseText));
                                    console.log(redirectId)
                                } else {
                                    console.log(oReq.status)
                                }
                            };

                            oReq.send(oData);
                            ev.preventDefault();

                        }, false);

                        function addToDelete(id) {
                            deleteImageIds.push(id);
                            $("#image".concat(id)).remove();
                        }

                        function getDataFromServer(id) {

                            $("#parrent".concat(id)).addClass("ro_add_color");
                            $("#parrent".concat(parentRentalUnit)).removeClass("ro_add_color");
                            if (parentRentalUnit == id) {
                                parentRentalUnit = -1;
                                $("#address").removeClass("ro_display_none");
                                $("#parrent_address").addClass("ro_display_none")

                            }
                            else {
                                parentRentalUnit = id;
                                $("#address").addClass("ro_display_none");
                                $("#parrent_address").removeClass("ro_display_none");
                            }
                            var send = [parentRentalUnit, currentRental];
                            <c:url var="ajaxParrent" value="/manage/rentalUnit/manageCurrentRentalUnit/getData"/>
                            $.ajax({
                                type: "POST",
                                contentType: "application/json",
                                url: "${ajaxParrent}",
                                data: JSON.stringify(send),
                                dataType: 'json',
                                timeout: 100000,
                                success: function (data) {
                                    console.log("SUCCESS: ", data);
                                    if (parentRentalUnit != -1) {
                                        if (data.country != undefined)
                                            $("#country_m").text("Country " + data.country);
                                        else $("#country_m").text("Country is'nt present");
                                        if (data.region != undefined)
                                            $("#region_m").text("Region " + data.region);
                                        else $("#region_m").text("Region is'nt present");
                                        if (data.zupCode != undefined)
                                            $("#zip_m").text("Zip Code " + data.zupCode);
                                        else  $("#zip_m").text("Zip Code is'nt present");
                                    }
                                    {
                                        $("#delAMen").remove();
                                        var str = '';
                                        jQuery.each(data.amenities, function (i, val) {

                                            if (data.parent[i]) {

                                                str += '<h6>' + data.amenities[i] + '';
                                                if (data.prices[i] != null)
                                                    str += ' ' + data.prices[i] + '$';
                                                else
                                                    str += 'Free</h6>';
                                            }
                                            else {
                                                str += '<label for="amenity' + data.amenities_id[i] + '" id="labelAmenity' + +data.amenities_id[i] + '">' + data.amenities[i] +
                                                        '<input type="checkbox" id="amenity' + data.amenities_id[i] + '" value="' + data.amenities_id[i] + '"' +
                                                        ' name="rentalAmenities[' + i + '].amenity.id"' +
                                                        ' onclick="addPriceField(' + data.amenities_id[i] + ',' + i + ')"';
                                                if (data.check[i]) {
                                                    str += 'checked';
                                                    str += ' ></label><input type="text" id="inputAmenity' + data.amenities_id[i] + '" value="' + data.prices[i] + '" ' +
                                                            ' name="rentalAmenities[' + i + '].price"/>';
                                                }
                                                else str += '></label>'
                                            }


                                        });
                                        $("#insertAmenity").append(function () {
                                            return '<div id="delAMen">' + str + '</div>';
                                        });

                                    }


                                },
                                error: function (e) {
                                    console.log("ERROR: ", e);

                                },
                                done: function (e) {
                                    console.log("DONE");
                                }
                            });

                        }

                    </script>


</t:template>