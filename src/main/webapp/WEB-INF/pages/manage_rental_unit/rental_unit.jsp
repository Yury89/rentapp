<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<t:template>

    <c:url var="url" value="/manage/rentalUnit/unit"/>
    <form method="post" action="${url}">

        <div class="row">
            <div class="input-field col s12">

                <input type="hidden" name="id" value="${rentalUnit.id}">
                <input type="text" name="name" id="name" value="${rentalUnit.name}"/>
                <label for="name">Name</label>

                <input type="text" name="title" id="title" value="${rentalUnit.title}">
                <label for="title">Title</label>

                <textarea name="description" id="description">${rentalUnit.description}</textarea>
                <label for="description"></label>
            </div>
        </div>
        <button type="submit">Submit</button>
    </form>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
    <script>
        $(document).ready(function () {
            $('input#input_text, textarea#description').characterCounter();
        });
    </script>

</t:template>