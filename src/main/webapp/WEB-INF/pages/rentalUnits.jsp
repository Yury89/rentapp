<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<t:template>

    <div class="row">
        <nav aria-label="You are here:" role="navigation">
            <ul class="breadcrumbs">
                <li><a href="<c:url value="/"/>">Landlord Home</a></li>
                <li><a href="#">Contact Landlord</a></li>
                <li><a href="#">Prepare CraigsList Ad</a></li>


            </ul>
        </nav>
        <div class="medium-6 columns">
                <%--<c:if test="${current.images.size() != 0}">--%>
                <%--<img class="thumbnail" src="<c:url value="/uploaded_image?name=${current.images.get(0).id}"/>">--%>
                <%--</c:if>--%>
                <%--<c:if test="${current.images.size() == 0}">--%>
                <%--<img class="thumbnail" src="<c:url value="/resources/no-photo.png"/>">--%>
                <%--</c:if>--%>
                <%--<div class="row small-up-4">--%>
                <%--<c:forEach items="${current.images}" begin="1" var="image">--%>
                <%--<div class="column">--%>
                <%--<img class="thumbnail" src="<c:url value="/uploaded_image?name=${image.id}"/>">--%>
                <%--</div>--%>
                <%--</c:forEach>--%>

                    <div class="fotorama" data-nav="thumbs">
                        <c:forEach items="${current.images}"  var="image">
                                <img  src="<c:url value="/uploaded_image?name=${image.id}"/>">
                        </c:forEach>
                    </div>
</div>
        <div class="medium-6 large-5 columns">
            <h3>${current.name}</h3>

            <c:if test="${current.parentRentalUnit != null}">
                <a href="<c:url value="/rentalunit?id=${current.parentRentalUnit.id}"/>">
                    <div class="media-object">
                        <div class="media-object-section">
                            <div class="thumbnail">

                                <c:if test="${current.parentRentalUnit.images.size() != 0}">
                                    <img src="<c:url value="/uploaded_image?name=${current.parentRentalUnit.images.get(0).id}"/>"
                                         width="100" height="100">
                                </c:if>
                                <c:if test="${current.images.size() == 0}">
                                    <img src="<c:url value="/resources/no-photo.png"/>" width="100" height="100">
                                </c:if>
                                Belongs to ${current.parentRentalUnit.name}
                            </div>

                        </div>

                    </div>
                </a>


            </c:if>
            <p> ${current.description}</p>

            <c:if test="${current.price.present}">


                <h5>Prices:</h5>
                <c:if test="${current.price.day != null}">
                    <p>For Day ${current.price.day}</p>
                </c:if>
                <c:if test="${current.price.week != null}">
                    <p>For Week ${current.price.week}</p>
                </c:if>
                <c:if test="${current.price.month != null}">
                    <p>For Month ${current.price.month}</p>
                </c:if>
            </c:if>
            <c:if test="${polices.size() != 0 || current.rentalPolicies.size() != 0}">
                <h5>Policies:</h5>
                <c:forEach items="${polices}" var="p">
                    <p class="ro_img">${p.name} <img src="<c:url value="/resources/question-speech-bubble.png"/>">
                    </p>
                    <div class="ro_hoverable">${p.description}</div>
                </c:forEach>
                <c:forEach items="${current.rentalPolicies}" var="p">
                    <p class="ro_img">${p.policy.name} <img
                            src="<c:url value="/resources/question-speech-bubble.png"/>">
                    </p>
                    <div class="ro_hoverable">${p.policy.description}</div>
                </c:forEach>
            </c:if>
            <c:if test="${current.policies != ''}">

                <h5>Additional Policy:</h5>
                <p>${current.policies}</p>
            </c:if>
            <c:if test="${amenities.size() != 0 || current.rentalAmenities.size() != 0}">
                <h5>Amenities</h5>
                <c:forEach items="${amenities}" var="amen">
                    <p>${amen.title}:
                        <c:if test="${amen.price!= null}">
                            ${amen.price}$
                        </c:if>
                        <c:if test="${amen.price == null}">
                            Free
                        </c:if>

                    </p>
                </c:forEach>
                <c:forEach items="${current.rentalAmenities}" var="amen">
                    <p>${amen.amenity.title}:
                        <c:if test="${amen.price!= null}">
                            ${amen.price}$
                        </c:if>
                        <c:if test="${amen.price == null}">
                            Free
                        </c:if>

                    </p>
                </c:forEach>
            </c:if>


            <sec:authorize access="hasRole('ADMIN')">
                <a href="<c:url value="/manage/rentalUnit/manageCurrentRentalUnit?id=${current.id}"/>"
                   class="button">Update</a>
            </sec:authorize>
            <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
        </div>


        <div class="column row">

            <ul class="tabs" data-tabs id="example-tabs">
            </ul>
            <div class="tabs-content" data-tabs-content="example-tabs">
                <div class="tabs-panel is-active" id="panel12">
                    <div class="row medium-up-3 large-up-5">
                        <c:forEach items="${child}" var="c">
                            <div class="column">
                                <c:if test="${c.images.size() != 0}">
                                    <img class="thumbnail"
                                         src="<c:url value="/uploaded_image?name=${c.images.get(0).id}"/>">
                                </c:if>
                                <c:if test="${c.images.size() == 0}">
                                    <img class="thumbnail" src="<c:url value="/resources/no-photo.png"/>">
                                </c:if>
                                <h5>${c.name}
                                    <small>
                                        <c:if test="${c.price.day != null}">
                                            $${c.price.day}
                                        </c:if>
                                    </small>
                                </h5>
                                <p>${c.description}</p>
                                <a href="<c:url value="/rentalunit?id=${c.id}"/>"
                                   class="button hollow tiny expanded">See</a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
            </div>
        </div>
                     <!-- 3 KB -->
        <script src="http://cdnjs.cloudflare.com/ajax/libs/fotorama/4.6.4/fotorama.js"></script> <!-- 16 KB -->
</div>
</t:template>