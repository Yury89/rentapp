<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<t:template>
    <c:url value="/j_spring_security_check" var="loginUrl"/>

    <div class="row">
        <form method="post" action="${loginUrl}">
            <sec:csrfInput/>
            <div class="row">
                <div class="large-3 large-offset-4">
                    <label>Login
                        <input type="text" name="login">
                    </label>
                </div>
                <div class="large-3 large-offset-4">
                    <label>Password
                        <input type="password" name="password">
                    </label>
                </div>
                <div class="large-offset-4">
                    <button type="submit" class="success button">Login</button>
                </div>
            </div>
        </form>
    </div>
    <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
</t:template>