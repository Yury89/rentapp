$(document).ready(function () {
    var exForName = "Start with large letter";
    // Устанавливаем обработчик потери фокуса для всех полей ввода текста
    $('input#name, input#title, textarea#description, input#price_day,input#price_week,input#price_month,input#address_coutry,input#address_region,input#address_zip_code, input#full_address').unbind().blur(function () {

        // Для удобства записываем обращения к атрибуту и значению каждого поля в переменные
        var id = $(this).attr('id');
        var val = $(this).val();

        switch (id) {
            case 'name':
                var regex = /^[A-Z]+[\w |]*/;
                if (val.length != 0 && !regex.test(val)) {

                    $(this).next('.valName').text(exForName).css('color', 'red');
                    break;
                }
                if (val.length > 45) {

                    $(this).next('.valName').text('Max 45 letters').css('color', 'red');
                    break;
                }
                if(val.length != 0) {

                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;

            case 'title':
                
                var regex = /^[A-Z]+[\w |]*/;
                if (val.length != 0 && !regex.test(val)) {

                    $(this).next('.valName').text(exForName).css('color', 'red');
                    break;
                }
                if (val.length > 45) {

                    $(this).next('.valName').text('Max 45 letters').css('color', 'red');
                    break;
                }
                if(val.length != 0) {

                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;

            case 'description':
                if (val.length == 0) {
                    $(this).next('.valName').text('the field required').css('color', 'red');
                    break;
                }
                if (val.length > 300) {
                    $(this).next('.valName').text('max 300 letters').css('color', 'red');
                    break;
                }

                else {
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;

            case 'price_day':
                var testRegEx = /^[\d]{1,10}(\.[\d]{1,2})?$/;
                if (!testRegEx.test(val) && val.length != 0) {
                    $(this).next('.valName').text('enter correct price').css('color', 'red');
                    break;
                }
                if(val.length == 0) {

                    $(this).next('.valName').text('').css('color', 'green');
                    break;
                }

                else {
                    if(val.length != 0)
                        $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;
            case 'price_week':
                var testRegEx = /^[\d]{1,10}(\.[\d]{1,2})?$/;
                if (!testRegEx.test(val) && val.length != 0) {
                    $(this).next('.valName').text('enter correct price').css('color', 'red');
                    break;
                }
                if(val.length == 0) {

                    $(this).next('.valName').text('').css('color', 'green');
                    break;
                }

                else {
                    if(val.length != 0)
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;
            case 'price_month':
                var testRegEx = /^[\d]{1,10}(\.[\d]{1,2})?$/;
                if (!testRegEx.test(val) && val.length != 0) {
                    $(this).next('.valName').text('enter correct price').css('color', 'red');
                    break;
                }
                if(val.length == 0) {

                    $(this).next('.valName').text('').css('color', 'green');
                    break;
                }

                else {
                    if(val.length != 0)
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;

            case 'address_coutry':
                var regex = /^[a-zA-Z]+$/;
                if (!regex.test(val) && val.length != 0) {
                    $(this).next('.valName').text('Enter Correct Country').css('color', 'red');
                    break;
                }

                else {
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;

            case 'full_address':
                var regex = /^[0-9]+[\w |]*/;
                if (val.length != 0 && !regex.test(val)) {

                    $(this).next('.valName').text('Start with number example - 7584 Big Canyon Drive').css('color', 'red');
                    break;
                }
                if (val.length > 45) {

                    $(this).next('.valName').text('Max 45 letters').css('color', 'red');
                    break;
                }
                
                if(val.length == 0) {

                    $(this).next('.valName').text('').css('color', 'green');
                    break;
                }
                else {

                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;

            case 'address_zip_code':
                var regex = /^\d{5}(?:[-\s]\d{4})?$/;
                if (!regex.test(val) && val.length != 0) {
                    $(this).next('.valName').text('Enter Correct Zip Code').css('color', 'red');
                    break;
                }
                if(val.length == 0) {

                    $(this).next('.valName').text('').css('color', 'green');
                    break;
                }

                else {
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;
            case 'address_region':
                var regex = /^[a-zA-Z]+$/;
                if (!regex.test(val) && val.length != 0) {
                    $(this).next('.valName').text('Enter Correct Street').css('color', 'red');
                    break;
                }

                else {
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;
            case 'adress_homenumber':
                var regex = /^[0-9]+$/;
                if (!regex.test(val) && val.length != 0) {
                    $(this).next('.valName').text('Enter Correct Home Number').css('color', 'red');
                    break;
                }

                else {
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;
            case 'adress_room':
                var regex = /^[0-9]+$/;
                if (!regex.test(val) && val.length != 0) {
                    $(this).next('.valName').text('Enter Correct Room').css('color', 'red');
                    break;
                }

                else {
                    $(this).next('.valName').text('ok').css('color', 'green');
                    break;
                }
                break;


        }

    })
});